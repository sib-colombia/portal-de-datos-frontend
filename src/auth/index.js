import fetch from 'isomorphic-fetch'

import { USER, URL_USUARIOS, TOKEN } from '../config/const'

// eslint-disable-next-line
let rootMe = null

export function http(type, data) {
  /*
    console.log(data)
  
    var body = new FormData();
    if (data)
      for (var key in data)
        body.append(key, data[key]);
  
    data = data
      ? body
      : null;*/

  if (data && data.username)
    data = 'username=' + data.username + '&password=' + data.password + '&email=' + data.email;

  let config = {
    method: type,
    // mode: 'cors',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': getToken()
    },
    body: data
  }

  return config;

}

export function signin(user) {
  return fetch(`${URL_USUARIOS}/auth/local`, http('POST', user)).then((response) => {
    return response.json()
  }).then((data) => {
    if (data.token) {
      return setToken(data).then(token => {
        return token;
      });
    } else {
      console.log("Retornando mensaje error ", data)
      if(data.message!==undefined){
        return { error: data.message }
      }else if(data.error!==""){
        console.log("enviando data.error")
        return { error: data.error }
      }else{
        console.log("enviando data completo")
        return data
      }
    }
  }).catch(err => {
    console.log("Error signin ", err)
    return err;
  })
}

export function logout() {
  clean();
  window.location.reload()
}

export function signup(user) {
  return fetch(`${URL_USUARIOS}/api/user`, http('POST', user)).then((response) => {
    return response.json()
  }).then((data) => {
    if (data && data.message && data.message.errors) {
      data = data.message.errors
      return { error: data.username.message }
    } else {
      return data
    }
  }).catch(error => {
    console.log(error)
    return error;
  })
}


export function me() {
  console.log("Consultando sesión del usuario")
  return fetch(`${URL_USUARIOS}/api/user/me`, http('GET')).then((response) => {
    return response.json()
  }).then((data) => {

    console.log("Consultando sesión del usuario ", data)
    if (data._id) {
      return setUser(data).then(user => {
        return user;
      });
    } else {
      throw new Error(`error: ${data.message}`)
    }

  }).catch(err => {
    clean();
  })

}

export function isAuthenticated() {
  return getUser()
}

export function setUser(data) {
  try {
    localStorage.setItem(USER, JSON.stringify(data));
  } catch (err) {
    return Promise.reject(err.message);
  }
  return Promise.resolve(rootMe = data || null);
}

export function getUser() {
  return JSON.parse(localStorage.getItem(USER))
}

export function getToken() {
  return localStorage.getItem(TOKEN);
}

export function setToken(data) {
  try {
    localStorage.setItem(TOKEN, data.token);
  } catch (err) {
    return Promise.reject(err.message);
  }
  return Promise.resolve(data.token || null);
}

export function clean() {
  localStorage.clear();
}

