import fetch from 'isomorphic-fetch';

import { URL_USUARIOS } from '../config/const';

export function getPage(id) {
  return fetch(`${URL_USUARIOS}/api/static/get/portal_${id}`, { method: 'GET' }).then((response) => {
    return response.json();
  }).then((data) => {
    return data;
  })

/*
  return fetch(`${URL_API}/api/dataset/list/${offset}/${type}/${q}`, { method: 'GET' }).then((response) => {
    return response.json();
  }).then((data) => {
    return data;
  })
*/
}
