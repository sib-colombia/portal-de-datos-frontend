//import { map, capitalize } from 'lodash'

import { URL_BUSQUEDAS, KEY_TYPE } from '../config/const'
import { request } from '../util'

export function getFacet(xhr, facet, args = '', search) {

  let kt = `keyType=${KEY_TYPE}`
  /*
  if ([
        "country",
        "county",
        "locality",
        "colombia",
        "departament_name",
        "municipio",
      ].indexOf(facet)!==-1){
    kt=""
  }*/

  let url = `${URL_BUSQUEDAS}/api/agg?agg=${facet}&${kt}&${search}`
  //let url = `${URL_ES}/api/resource/facets?${kt}facet[]=${facet}&limit=10`

  if (args !== '')
    url += args

  if (search === 'undefined')
    search = ''

  if (search && search !== '') {
    if (search.indexOf('?') === 0) {
      search = search.slice(1)
    }
    url += '&' + search
  }

  return request(xhr, 'GET', url)
    .then((data) => {
    return data;
    /*
      console.log("Están llegando los datos del facet: ", data.aggregations)
      const [indice] = Object.keys(data.aggregations)
      console.log("-> ", indice)
    
    
      return data.aggregations[indice]["buckets"];


      map(, (v, k) => {
        /*
        let counts = {}
        let nombre = v.stats_facet.slice(6)
        if (nombre.indexOf('_') > -1) {
          let spl = nombre.split('_')

          if (nombre.match(/^basis.*$/)) {
            nombre = spl[0] + 'Of' + capitalize(spl[1])
          } else if (nombre.match(/^taxon.*$/)) {
            nombre = spl.join('_')
          } else if (nombre.match(/^resource.*$/)) {
            nombre = spl[0] + '_' + spl[1]
          } else {
            //console.log(nombre)
          }
        }
        map(v.values, (v2, k2) => {
          let nombre2 = v2._id[nombre]
          if (nombre2 === "")
            nombre2 = "Sin_Nombre"
          if(nombre2!=='undefined'){
            nombre2 = nombre2.toLowerCase().replace(/^(.)|\s(.)/g, ($1) => $1.toUpperCase()) 
            counts[nombre2] = {
              "count": v2.value ? v2.value : 0,
              "fraction": v2.average ? v2.average : 0,
              "title": nombre2 ? nombre2 : "sin_nombre_" + k + "_" + k2
            }
          }
        })
        facets[nombre.toUpperCase()] = {
          "counts": counts
        }
      })
        */

    })
}


export function getAuto(xhr, facet, search) {

  let url = `${URL_BUSQUEDAS}/api/auto?campo=${facet}&keyType=${KEY_TYPE}&valor=${search}`

  return request(xhr, 'GET', url)
    .then((data) => {
        return data;
    })
}
