import { URL_BUSQUEDAS, KEY_TYPE } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function getSpeciesList(limit, search) {
  if (search && search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_BUSQUEDAS}/api/agg2?agg=scientificName&keyType=${KEY_TYPE}&limit=${limit}${search && '&' + search}`)
}




