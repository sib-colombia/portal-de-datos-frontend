import fetch from 'isomorphic-fetch';
import { URL_CONSULTAS, URL_BUSQUEDAS, KEY_TYPE, URL_DESCARGAS } from '../config/const';
import { request } from '../util'

const xhr = new XMLHttpRequest()

export function ESgetOccurrenceList(offset, search) {
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return request(xhr, 'GET', `${URL_BUSQUEDAS}/api/search?keyType=${KEY_TYPE}&page=${offset}${search && '&' + search}`)
}

export function descargar(search, usuario, currentUrl) {
  if (search.indexOf('?') >= 0) {
    search = search.slice(1)
  }

  return fetch(`${URL_DESCARGAS}/api/newdownload?email=${usuario}${search && '&' + search}&keyType=${KEY_TYPE}&currentUrl=${currentUrl}`, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  });
}

export function getOccurrence(id) {
  return fetch(`${URL_CONSULTAS}/api/occurrence/` + id, {
    method: 'GET'
  }).then((response) => {
    return response.json()
  }).then((data) => {
    return data
  })
}
