import React, {Component} from 'react';
import map from 'lodash/map';

import DatasetsListItem from '../molecules/DatasetsListItem';
import Loading from '../atoms/Loading';

class DatasetsList extends Component {
  render() {
    const {data} = this.props;
    return (<div className="uk-container uk-container-small">
      {(data && <div className="uk-flex uk-flex-column">
        {map(data, (dataset, key) => (<DatasetsListItem key={key} data={dataset}/>))}
      </div>) || <Loading />}
    </div>)
  }
}

export default DatasetsList;
