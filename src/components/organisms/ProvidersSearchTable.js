import React, { Component } from 'react';
import map from 'lodash/map';

import ProvidersRow from '../molecules/ProvidersRow';
import Loading from '../atoms/Loading';
import * as ProviderService from '../../services/ProviderService';

class ProvidersSearchTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      count: NaN,
      show: false,
      buffer: null,
      limite: 20,
      search: ""
    }
  }

  componentDidMount() {
    this.offsetResults();
  }

  onChangePage(limite) {
    this.offsetResults(limite);
  }

  cargarMas(){
    this.setState({limite: this.state.limite+20}, ()=>{
      this.offsetResults()
    })
  }

  offsetResults(search) {
    if (search === undefined)
      search = this.props.search
      console.log("search es " + this.props.search)
      console.log("la búsqueda es:" + search)

    ProviderService.getProviderSearchList(this.state.limite, search).then(data => {
      console.log("aquí estoy")
      this.setState({
          data: data.aggregations.organizationId.buckets,
          count: data.aggregations.organizationId.doc_count_error_upper_bound,
          actualPage: 0
        })
      this.props.providers(data.count);
    });
  }

  render() {

    let t
    if (this.state.data===null){
    t = 0
    }else{
      t = this.state.data.length
    }

    return (
      <li>
        {(this.state.data && <div className="uk-card uk-card-default uk-overflow-auto uk-margin">
          <table className="uk-table uk-table-divider uk-table-small uk-table-hover">
            <thead>
              <tr>
                <th>Publicador</th>
                <th className="uk-width-small">Registros</th>
                {/*<th className="uk-width-small">Recursos</th>*/}
              </tr>
            </thead>
            <tbody>
              {this.state.data && map(this.state.data, (v, k) => (<ProvidersRow key={k} data={v} />))}
            </tbody>
          </table>
        </div>) || <Loading />}
        {t===this.state.limite &&
          <div className="uk-align-center uk-button uk-button-primary" onClick={()=>{this.cargarMas()}}>Cargar mas de {t} {this.state.cargando}</div>
        }
      </li>
    )
  }
}

export default ProvidersSearchTable;
