import React, { Component } from 'react';
import UIkit from 'uikit';

export class Offcanvas extends Component {

  componentWillUnmount() {
    UIkit.offcanvas('#my-id').$destroy(true);
  }

  render() {
    return (
      <div id="my-id" onClick={(e) => e.preventDefault()} data-uk-offcanvas="" style={{ marginTop: 70, overflowY: "scroll" }}>
        <div className="uk-offcanvas-bar2 uk-padding-remove uk-background-default" style={{ borderTop: 'solid 2px #ff7847' }}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Offcanvas;
