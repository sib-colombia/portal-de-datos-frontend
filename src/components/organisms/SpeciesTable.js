import React, { Component } from 'react';

import SpeciesRow from '../molecules/SpeciesRow';
import * as SpeciesService from '../../services/SpeciesService';
import Loading from '../atoms/Loading';
import map from "lodash/map";

class SpeciesTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      count: NaN,
      show: false,
      buffer: null,
      limite: 20,
      search: ""
    }
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.offsetResults();
  }

  onChangePage(limite) {
    this.offsetResults(limite);
  }

  cargarMas(){
    this.setState({limite: this.state.limite+20}, ()=>{
      this.offsetResults()
    })
  }

  offsetResults(search) {
    if (search === undefined)
      search = this.props.search

    SpeciesService.getSpeciesList(this.state.limite, search).then(data => {
      this.setState({
        data: data.aggregations.scientificName.buckets,
        count: data.aggregations.scientificName.doc_count_error_upper_bound,
        actualPage: 0
      })
      this.props.species(data.count);
    });

  }

  render() {

    let t
    if (this.state.data===null){
    t = 0
    }else{
      t = this.state.data.length
    }

    return (
      <li>
        {(this.state.data && <div className="uk-card uk-card-default uk-overflow-auto uk-margin">
          <table className="uk-table uk-table-divider uk-table-small uk-table-hover">
            <thead>
              <tr>
                <th>Registros</th>
                <th>Nombre Cientifico</th>
{/*             <th>Reino</th>
                <th>Filo</th>
                <th>Clase</th>
                <th>Orden</th>
                <th>Familia</th>*/}
                <th>Genero</th>
                <th className="uk-text-nowrap">Epíteto Específico</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data && map(this.state.data, (especie, key) => (<SpeciesRow key={key} data={especie}/>))}
            </tbody>
          </table>
        </div>) || <Loading />}
        {t===this.state.limite &&
          <div className="uk-align-center uk-button uk-button-primary" onClick={()=>{this.cargarMas()}}>Cargar mas de {t} {this.state.cargando}</div>
        }
      </li>
    );
  }
}

export default SpeciesTable;
