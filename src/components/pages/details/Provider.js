import React, { Component } from 'react';
import { Link } from 'react-router-dom';
//import { TileLayer } from 'react-leaflet';
import { get, map } from 'lodash';

import MarkerClusterGroup from 'react-leaflet-markercluster';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import HumboldtMap from '../../molecules/HumboldtMap';
import GeneralStatisticsCont from '../../atoms/GeneralStatisticsCont';
import Loading from '../../atoms/Loading';
import ContactsGrid from '../../organisms/ContactsGrid';

import * as ProviderService from '../../../services/ProviderService';
import * as MapService from '../../../services/MapService';



class Provider extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: null,
      cargaMapaCompleta: false,
    }
  }
  
  getMapPoints(params, total, pagina){
      //console.log("Consultando los puntos del mapa: ", params, total, pagina)
      if (total===undefined){
        pagina = 0
      }else{
        console.log("Verificando final: puntos en mapa: ", this.state.dataMapa.length)
        console.log("total: ", total)
        if (total<=this.state.dataMapa.length){
          this.setState({"cargaMapaCompleta": true})
          return;
        }
        //console.log("consultando siguientes puntos")
      }
      
      let p  = params.slice()
      let cantidad = 100
      p.push("limit="+cantidad)
      p.push("page="+pagina)
    
      MapService.getOccurrenceList(p)
                .then(dataOrg => {
                  if(dataOrg.values !== undefined && dataOrg.values.length > 0) {
                    let data = dataOrg.values
                    let markers = [];
                    if(this.state.dataMapa)
                      markers = this.state.dataMapa;
                      
                    map(data, (v, k) => {
                      //let coordinates = get(v, 'location.coordinates')
                      
                      let lat = parseFloat(v.decimalLatitude)
                      let lon = parseFloat(v.decimalLongitude)
                   
                      if(isNaN(lat) || isNaN(lon)){
                        console.log("El registro no tiene bien los puntos:", k)
                        console.log(v)
                      }else{
                        markers.push({ 
                          position: [
                            parseFloat(lat), parseFloat(lon)
                          ], 
                          popup: (`
                            <div>
                              <h4>Registro</h4>
                              <a href="/occurrence/${v.id}" target="_blank" class="uk-link-reset" >${v.scientificName}</a><hr/>
                              <h4>Recurso</h4>
                              <a href="/dataset/${v.GBIFID}" target="_blank" class="uk-link-reset" >${v.resource_name}</a><hr/>
                              <h4>Publicador</h4>
                              <a href="/provider/${v.provider_id}" target="_blank" class="uk-link-reset" >${v.provider_name}</a>
                            </div>
                          `) 
                        })
                      }
                    })
                    console.log()
                    this.setState({ "dataMapa": markers, "dataMapaTotal": dataOrg.count }, ()=>{
                      console.log("Voy a consultar el siguiente nivel")
                      console.log(this.state)
                      this.getMapPoints(params, this.state.dataMapaTotal, pagina+1)
                    })
                  }else{
                    //alert("No se encontraron puntos")
                  }
                })
  }
  

  componentDidMount() {
    if (this.props.match.params.id !== undefined) {
      ProviderService.getProvider(this.props.match.params.id).then(data => {
        this.setState({ data: data }, ()=>{
          this.getMapPoints(["provider_id="+this.props.match.params.id])
        })
      }).catch(err => {
        console.log(err)
      })
    }
  }

  render() {
    let { data } = this.state;
    if (data)
      data = data.provider
    //console.log(data)
    let anchoCargaMapa=0;
    if (this.state.dataMapa)
      anchoCargaMapa=parseInt((this.state.dataMapa.length/this.state.dataMapaTotal)*100, 10)+"%"
      
    return (
      <GenericPage
        titlep="Búsqueda - Portal de datos"
        header={<Header />}
        footer={<Footer />}
      >
        {(data && <div className="uk-section uk-padding-remove-bottom">
          {   console.log(data.key) }
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid>
            <div>
              <h4 className="uk-margin-left uk-heading-divider uk-margin-remove" style={{ borderBottomColor: '#ff7847' }}>PUBLICADOR</h4>
            </div>
          </div>
          <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid>
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-padding-remove uk-margin-top">
              <h2>{data.title}</h2>
            </div>
          </div>

          <div className="uk-margin-top">
            <GeneralStatisticsCont id={"organizationId"} param={this.props.match.params.id} providerId={ data.key } />
          </div>
          <div className="uk-flex uk-flex-center uk-margin-top uk-margin">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid>
                <div className="uk-width-1-4">
                  <Link to=""><img className="uk-preserved-width" src={data.resourceLogoUrl} alt="" /></Link>
                  <p className="uk-text-bold"><span className="uk-text-top uk-margin-small-right" uk-icon="icon: link"></span><a className="uk-text-tertiary" href={get(data, 'homepage')} target="_blank" rel="noopener noreferrer" >{get(data, 'homepage')}</a></p>
                </div>
                <div>
                  <p>{data.description}</p>
                  <p className="uk-text-bold"><span className="uk-margin-small-right">Herramienta de publicación</span> <Link className="uk-text-tertiary" to=" ">Enlace</Link></p>
                </div>
              </div>
            </div>
          </div>
          <div className="uk-section uk-section-small uk-section-default">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Contactos</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
              </div>
              <ContactsGrid data={data.contacts} grid="1-3" />
            </div>
          </div>
          <div className="uk-section uk-section-small uk-padding-remove-bottom">
            <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
              <div className="uk-flex uk-flex-column">
                <h3 className="uk-margin-left uk-text-bold">Registros con coordenadas</h3>
                <div className="uk-width-1-4 uk-width-small@s uk-background-primary" style={{ height: 2 }}></div>
              </div>
            </div>
            <div className="uk-height-large uk-margin-top">
                  {!this.state.cargaMapaCompleta &&
                    <div className="barraCarga" style={{"height": "2px", "width": anchoCargaMapa, backgroundColor: "#ff7847"}}></div>
                  }
                  <HumboldtMap zoom={5}>
                    {this.state.cargaMapaCompleta && <MarkerClusterGroup markers={this.state.dataMapa} />}
                  </HumboldtMap>
            </div>
          </div>
        </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>}
      </GenericPage>
    );
  }
}

export default Provider;
