import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { map } from 'lodash';
import { route } from '../../../util';

import MarkerClusterGroup from 'react-leaflet-markercluster';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import Doi from '../../atoms/Doi';
import License from '../../atoms/License';
import GeneralStatisticsCont from '../../atoms/GeneralStatisticsCont';
import DataSheet from '../../molecules/DataSheet';
import Loading from '../../atoms/Loading';
import ContactsGrid from '../../organisms/ContactsGrid';
//import OccurrencesTable from '../../organisms/OccurrencesTable';
import GeographicCoverages from '../../molecules/GeographicCoverages';
import HumboldtMap from '../../molecules/HumboldtMap';

import * as DatasetService from '../../../services/DatasetService';
//import * as MapService from '../../../services/MapService';


class Dataset extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  componentDidMount() {
    if (this.props.match.params.id !== undefined) {
      DatasetService.getDataset(this.props.match.params.id).then(data => {
        /*if (data.values.length===0){
          alert("No se encuentra el recurso en la base de datos.")
          window.location.href="/"
        }
        */
        this.setState({ data: data }, ()=>{
              /*
              MapService.getOccurrenceList(["gbifId="+this.props.match.params.id])
                .then(data => {
                  if(data.values !== undefined && data.values.length > 0) {
                    data = data.values
                    let markers = []
                    map(data, (v, k) => {
                      //let coordinates = get(v, 'location.coordinates')
                      
                      let lat = parseFloat(v.decimalLatitude)
                      let lon = parseFloat(v.decimalLongitude)
                   
                      if(isNaN(lat) || isNaN(lon)){
                        console.log("El registro no tiene bien los puntos:", k)
                        console.log(v)
                      }else{
                        markers[k] = { 
                          position: [
                            parseFloat(lat), parseFloat(lon)
                          ], 
                          popup: (`
                            <div>
                              <h4>Registro</h4>
                              <a href="/occurrence/${v.id}" target="_blank" class="uk-link-reset" >${v.scientificName}</a><hr/>
                              <h4>Recurso</h4>
                              <a href="/dataset/${v.GBIFID}" target="_blank" class="uk-link-reset" >${v.resource_name}</a><hr/>
                              <h4>Publicador</h4>
                              <a href="/provider/${v.provider_id}" target="_blank" class="uk-link-reset" >${v.provider_name}</a>
                            </div>
                          `) 
                        }
                      }
                    })
                    this.setState({ dataMapa: markers })
                  }else{
                    alert("No se encontraron puntos")
                  }
                })
        */
        })
      }).catch(err => {
        console.log(err)
      })
    }
  }

  taxonomyCoverage(data) {
  
    console.log("taxonomyCoverage")
    console.log(data)
    console.log(Array.isArray(data))
    
    if(Array.isArray(data)){
      return map(data, (v, k) => {
        return (
          <div key={k} className="uk-padding-small">
            <h4 className="uk-text-bold uk-margin-remove">{v.generalTaxonomicCoverage}</h4>
            {this.taxonomicClassification(v.taxonomicClassification)}
          </div>
        )
      })
    }else{
        return (
          <div className="uk-padding-small">
            {data.generalTaxonomicCoverage &&
              <h4 className="uk-text-bold uk-margin-remove">{data.generalTaxonomicCoverage}</h4>
            }
            {this.taxonomicClassification(data.taxonomicClassification)}
          </div>
        )
    }
  }

  taxonomicClassification(data) {
  
    console.log("taxonomicClassification")
    console.log(data)
    console.log(Array.isArray(data))
  
    let groups = [];
    if(Array.isArray(data)){
      map(data, (v, k) => {
        if (typeof groups[v.taxonRankName] === 'undefined') { groups[v.taxonRankName] = []; }
        groups[v.taxonRankName].push(v.taxonRankValue)
      })
    }else{
      if (typeof groups[data.taxonRankName] === 'undefined') { groups[data.taxonRankName] = []; }
      groups[data.taxonRankName].push(data.taxonRankValue)
    }

    for (const key in groups) {
      if (groups.hasOwnProperty(key)) {
        const element = groups[key];
        return (
          <div key={key} className="uk-margin-small-top">
            <h4 className="uk-margin-remove">{key}</h4>
            {
              <div className="uk-column-1-3 uk-padding-small">
                {
                  map(element, (value, index) => (
                    <div key={index}>
                      <a className="uk-text-tertiary" href={`/search?${key}=${value}`} target="_blank" rel="noopener noreferrer" >{value}</a>
                    </div>
                  ))
                }
              </div>
            }
          </div>
        )
      }
    }
  }

  handleIdentifiers(data) {
    return data.map((value, key) => {
      return (value.type === 'DOI' && <Doi key={key} label={value.identifier} />) || ((value.type === 'UUID' && <p key={key}>
        <Link to={`/dataset/${value.identifier}`} className="uk-text-tertiary">{`${window.location.origin}/dataset/${value.identifier}`}</Link><br />
        <a href={`https://www.gbif.org/dataset/${value.identifier}`} className="uk-text-tertiary" target="_blank" rel="noopener noreferrer" >https://www.gbif.org/dataset/{value.identifier}</a>
      </p>)) || (value.type && <p key={key}>
        <a href={value.identifier} target="_blank" rel="noopener noreferrer" className="uk-text-tertiary">{value.identifier}</a>
      </p>)
    })
  }

  descargar(url) {
    window.open(url);
  }

  temporalCoverage(data) {
    console.log("los datos son" + Object.values(data))
    if(data.rangeOfDates === undefined) 
    
    return (
      <p>
        <span className="uk-margin-small-left uk-margin-small-right"> No se han encontrado rangos de fecha para este recurso </span>
      </p>
    )
    
    data = data.rangeOfDates;
    const beginDate = data.beginDate.calendarDate;
    const endDate = data.endDate.calendarDate;

    return (
      <p>
        <span className="uk-text-bold uk-margin-small-left uk-margin-small-right">Inicio:</span>
        {beginDate}
        <span data-uk-icon="icon: arrow-right" />
        <span className="uk-text-bold uk-margin-small-right">Fin:</span>
        {endDate}
      </p>
    )
  }
  
  imprimirMetodos(ms){
    if (Array.isArray(ms)){
      let lista = map(ms, (v, k)=>{
        return <p key={k}>{v.description.para}</p>
      })
      return lista
    }
    return <p>{ms.description.para}</p>

  }
  
  
  _t(x){
    if( (typeof x === "object") && (x !== null) )
    {
      if( x["#text"]!==undefined)
      return x["#text"]
    }
    return x
  }


  render() {
    const { data } = this.state;
    
    console.log("Dataset")
    console.log(data)
    
    let org = []
    if(data){
      org = data.eml.contact
      
      if (!Array.isArray(org))
        org = [org]
        

    }
    
    
    return (
      <GenericPage titlep="Búsqueda - Portal de datos" header={<Header />} footer={<Footer />}>
        {
          (data && <div className="uk-section uk-section">
            <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="">
              <div>
                <h4 className="uk-margin-left uk-heading-divider uk-margin-remove" style={{
                  borderBottomColor: '#ff7847'
                }}>RECURSO</h4>
              </div>
            </div>
            <div className="uk-flex uk-flex-center uk-grid-collapse" data-uk-grid="data-uk-grid">
              <div className="uk-container uk-width-5-6@m uk-width-2-3@l uk-text-center uk-margin-top uk-padding-remove">
                <h2>{data.eml.titleResource}</h2>
              </div>
              <div className="uk-width-1-1 uk-text-center uk-margin-small-top">
                <h5 className="uk-text-tertiary">Publicado por
                <Link className="uk-text-tertiary uk-margin-small-left" to={`/provider/${route(data, ['organizationId'])}`} style={{
                    textDecoration: 'underline'
                  }}>
                    {route(data, ['organization', 'provider', 'title']) && route(data, ['organization', 'provider', 'title'])}
                    {!route(data, ['organization', 'provider', 'title']) && "Click Acá"}
                  </Link>
                </h5>
              </div>
              <div className="uk-grid-small uk-margin-small-top" data-uk-grid="data-uk-grid">
                { map(org, (c, k)=>
                    <div key={k}>
                      <div>{c.individualName.givenName + ' ' + c.individualName.surName}</div>
                      <div>·</div>
                      <div><a className="uk-text-tertiary" href={'mailto:' + c.electronicMailAddress}>{c.electronicMailAddress}</a></div>
                    </div>
                )}
              </div>
            </div>
            <div className="uk-section uk-margin-large-top uk-heading-divider uk-padding-remove" style={{ borderBottomWidth: 1 }}>
              <div className="uk-container uk-container-small uk-padding-remove ">
                <div className="uk-grid-collapse uk-child-width-expand uk-margin uk-flex uk-flex-bottom" data-uk-grid="data-uk-grid">
                  <div>
                    <ul className="uk-margin-medium-top" data-uk-tab="swiping: false;" data-uk-switcher="connect: #paginasT">
                      <li>
                        <a>RECURSO</a>
                      </li>
                      <li>
                        <a>PROYECTO</a>
                      </li>
                    </ul>
                  </div>
                  <div className="uk-width-auto">
                    <ul className="uk-margin-medium-top" data-uk-tab="swiping: false;">
                      <li>
                        <a className="uk-text-right">
                          <span className="uk-text-top" data-uk-icon="icon: download"></span>
                            Descarga</a>
                        <div data-uk-dropdown="mode: hover">
                          {
                            data.eml_raw["eml:eml"]["dataset"]["alternateIdentifier"]
                              .map((value, key) => {
                                console.log("Value: ", value)
                                if (value.substring(0,4)==="http"){
                                  return (
                                    <ul className="uk-nav uk-dropdown-nav" key={key}>
                                      <li>
                                        <a className="uk-text-tertiary" href={value} target="_blank" onClick={() => { this.descargar(value.replace("resource", "archive")) }} rel="noopener noreferrer" >DwC-A</a>
                                      </li>
                                      <li>
                                        <a className="uk-text-tertiary" href={value.replace("resource", "rtf")} target="_blank" onClick={() => { this.descargar(value.replace("resource", "rtf")) }} rel="noopener noreferrer" >RTF</a>
                                      </li>
                                    </ul>
                                  )
                                }
                                return <div></div>
                              })
                          }
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <ul className="uk-switcher switcher-dataset" id="paginasT">
              <div className="uk-section uk-section-xsmall">
                <div className="uk-flex uk-flex-center">
                  <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                    <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid="data-uk-grid">
                      <div className="uk-width-auto">
                        <div style={{
                          width: 235,
                          height: 235
                        }} className="uk-flex uk-flex-center uk-flex-middle">
                          <Link to=""><img className="uk-preserved-width" width="150" height="150" src={data.eml.resourceLogoUrl} alt="" /></Link>
                        </div>
                        <Doi label={data.eml.doi} />
                      </div>
                      <div>
                        <p>{data.eml.abstract && data.eml.abstract.substring(0, 200)}...
                          <a href="#descripcion" data-uk-scroll="offset: 95" className="uk-text-tertiary" to="">Ver más</a>
                        </p>
                        <p className="uk-margin-small">
                          <span className="uk-text-bold uk-margin-small-right">Ultima modificacíon:</span>
                          {data.eml_raw["eml:eml"].dataset.pubDate}</p>
                        <div className="uk-margin-small">
                          <span className="uk-text-bold uk-margin-small-right">Licencia:</span>
                          <License id={data.eml.licence_url} /></div>
                        <p className="uk-margin-small">
                          <a className="uk-text-tertiary" href="#citacion" data-uk-scroll="offset: 90">¿Cómo citar?</a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="uk-margin-top">
                  <GeneralStatisticsCont id="gbifId" param={data.gbifId} />
                </div>
                <div className="uk-height-large uk-margin-remove-top">
                  <HumboldtMap zoom={5}>
                    {this.state.dataMapa && <MarkerClusterGroup markers={this.state.dataMapa} />}
                  </HumboldtMap>

                </div>
                <div className="uk-section uk-section-xsmall">
                  <div className="uk-flex uk-flex-center">
                    <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                      <div className="uk-grid-small" data-uk-grid="data-uk-grid">
                        <div className="uk-width-1-4">
                          <div className="uk-card uk-card-default uk-padding-small" data-uk-sticky="offset: 90; bottom: true" style={{ zIndex: 979 }}>
                            <ul className="uk-list uk-list-large uk-list-bullet uk-margin-remove-bottom" uk-scrollspy-nav="closest: li; scroll: true; cls: uk-text-primary; offset: 90">
                              <li>
                                <a className="uk-link-reset" href="#descripcion" data-uk-scroll="offset: 90">Descripción</a>
                              </li>
                              <li>
                                <a className="uk-link-reset" href="#cobertura_temporal" data-uk-scroll="offset: 90">Temporal</a>
                              </li>
                              <li>
                                <a className="uk-link-reset" href="#cobertura_geografica" data-uk-scroll="offset: 90">Geográfica</a>
                              </li>
                              <li>
                                <a className="uk-link-reset" href="#cobertura_taxonomica" data-uk-scroll="offset: 90">Taxonómica</a>
                              </li>
                              <li>
                                <a className="uk-link-reset" href="#metodologia" data-uk-scroll="offset: 90">Método de muestreo</a>
                              </li>
                              {
                                data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography &&
                                data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.length > 0 &&
                                <li>
                                  <a className="uk-link-reset" href="#bibliografia" data-uk-scroll="offset: 90">Bibliografía</a>
                                </li>
                              }
                              <li>
                                <a className="uk-link-reset" href="#partes_asociadas" data-uk-scroll="offset: 90">Partes asociadas</a>
                              </li>
                              <li>
                                <a className="uk-link-reset" href="#citacion" data-uk-scroll="offset: 90">Citación</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div className="uk-width-3-4">
                          <DataSheet scroll="descripcion" title="Descripción">
                            <p>{data.eml.abstract}</p>
                          </DataSheet>
                          {data.eml_raw["eml:eml"].dataset.coverage.temporalCoverage &&
                            <DataSheet scroll="cobertura_temporal" title="Cobertura temporal" className="uk-margin-top">
                              {
                                this.temporalCoverage(data.eml_raw["eml:eml"].dataset.coverage.temporalCoverage)
                              }
                            </DataSheet>
                          }
                          <DataSheet scroll="cobertura_geografica" title="Cobertura geográfica" className="uk-margin-top">
                            <p className="uk-margin-remove-bottom">{data.eml_raw["eml:eml"].dataset.coverage.geographicCoverage.geographicDescription}</p>
                          </DataSheet>
                          <div className="uk-height-large uk-box-shadow-medium">
                            <HumboldtMap zoom={5}>
                              <GeographicCoverages boundingCoordinates={data.eml_raw["eml:eml"].dataset.coverage.geographicCoverage.boundingCoordinates} />
                            </HumboldtMap>
                          </div>
                          {data.eml_raw["eml:eml"].dataset.coverage.taxonomicCoverage &&
                            <DataSheet scroll="cobertura_taxonomica" title="Cobertura taxonómica" className="uk-margin-top">
                              {this.taxonomyCoverage(data.eml_raw["eml:eml"].dataset.coverage.taxonomicCoverage)}
                            </DataSheet>
                          }
                          <DataSheet scroll="metodologia" title="Metodología" className="uk-margin-top">
                            <div className="uk-padding-small">
                              {
                                data.eml_raw["eml:eml"].dataset.methods.sampling &&
                                <div>
                                  {data.eml_raw["eml:eml"].dataset.methods.sampling.studyExtent.description.para && <h4 className="uk-text-bold uk-margin-small">Área de estudio</h4>}
                                  {data.eml_raw["eml:eml"].dataset.methods.sampling.studyExtent.description.para && <p>{data.eml_raw["eml:eml"].dataset.methods.sampling.studyExtent.description.para}</p>}
                                  {data.eml_raw["eml:eml"].dataset.methods.sampling.samplingDescription.para && <h4 className="uk-text-bold uk-margin-small">Muestreo</h4>}
                                  {data.eml_raw["eml:eml"].dataset.methods.sampling.samplingDescription.para && <p>{data.eml_raw["eml:eml"].dataset.methods.sampling.samplingDescription.para}</p>}
                                </div>
                              }
                              {data.eml_raw["eml:eml"].dataset.methods.qualityControl && data.eml_raw["eml:eml"].dataset.methods.qualityControl.description.para && <h4 className="uk-text-bold uk-margin-small">Control de calidad</h4>}
                              {data.eml_raw["eml:eml"].dataset.methods.qualityControl && data.eml_raw["eml:eml"].dataset.methods.qualityControl.description.para && <p>{data.eml_raw["eml:eml"].dataset.methods.qualityControl.description.para}</p>}
                              <h4 className="uk-text-bold uk-margin-small">Descripción de la metodología paso a paso</h4>
                              {data.eml_raw["eml:eml"].dataset.methods.methodStep &&

                                  <div className="uk-child-width-expand uk-grid-small" data-uk-grid="data-uk-grid">
                                    <div className="uk-width-auto">
                                      <span className="uk-badge uk-text-bold">*</span>
                                    </div>
                                    <div>
                                      {this.imprimirMetodos(data.eml_raw["eml:eml"].dataset.methods.methodStep)}
                                    </div>
                                  </div>
                              }
                            </div>
                          </DataSheet>
                          {
                            data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography &&
                            Array.isArray(data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation) &&
                              <DataSheet scroll="bibliografia" title="Bibliografía" className="uk-margin-top">
                                <ol>
                                  {data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation.map((value, key) => {
                                  
                                    if (typeof value === 'string'){
                                      return <li key={key}>{value}</li>
                                    }else{
                                      return <li key={key}>{value["#text"]}</li>
                                    }
                                  })}
                                </ol>
                              </DataSheet>
                          }
                          {
                            data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography &&
                            !Array.isArray(data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation) &&
                              <DataSheet scroll="bibliografia" title="Bibliografía" className="uk-margin-top">
                                {this._t(data.eml_raw['eml:eml'].additionalMetadata.metadata.gbif.bibliography.citation)}
                              </DataSheet>
                          }
                          { data.eml_raw["eml:eml"].dataset.associatedParty &&
                          <DataSheet scroll="partes_asociadas" title="Partes asociadas" className="uk-margin-top">
                            <ContactsGrid data={data.eml_raw["eml:eml"].dataset.associatedParty} grid="1-2" />
                          </DataSheet>
                          }
                          {/* <DataSheet scroll="registro_gbif" title="Registro en GBIF" className="uk-margin-top">
                            <table className="uk-table uk-table-small uk-margin-remove-bottom">
                              <tbody>
                                {
                                  data.created && <tr>
                                    <td className="uk-text-bold">Fecha de registro</td>
                                    <td className="uk-table-expand">{new Date(data.created).toLocaleDateString()}</td>
                                  </tr>
                                }
                                {
                                  data.modified && <tr>
                                    <td className="uk-text-bold">Última edición</td>
                                    <td className="uk-table-expand">{new Date(data.modified).toLocaleDateString()}</td>
                                  </tr>
                                }
                                {
                                  data.pubDate && <tr>
                                    <td className="uk-text-bold">Fecha de publicación</td>
                                    <td className="uk-table-expand">{new Date(data.pubDate).toLocaleDateString()}</td>
                                  </tr>
                                }
                                {
                                  data.publishingOrganization && <tr>
                                    <td className="uk-text-bold">Host</td>
                                    <td className="uk-table-expand">{data.publishingOrganization.title}</td>
                                  </tr>
                                }
                                <tr>
                                  <td className="uk-text-bold">Installation Contacts</td>
                                  <td className="uk-table-expand">
                                    <ContactsGrid minified="minified" data={data.contacts} />
                                  </td>
                                </tr>
                                <tr>
                                  <td className="uk-text-bold">Endpoints</td>
                                  <td className="uk-table-expand">
                                    {
                                    (data.endpoints).map((value, key) => (<p key={key}>
                                      <a className="uk-text-tertiary" href={value.url} target="_blank">{value.url}</a>
                                      ({value.type})</p>))
                                  }
                                  </td>
                                </tr>
                                <tr>
                                  <td className="uk-text-bold">Identifiers</td>
                                  {<td className="uk-table-expand">
                                  {this.handleIdentifiers(data.identifiers)}
                                </td>}
                                </tr>
                              </tbody>
                            </table>
                          </DataSheet> */}
                          <DataSheet scroll="citacion" title="Citación" className="uk-margin-top">
                            <p>{data.eml.citation}</p>
                          </DataSheet>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="uk-section uk-section-xsmall">
                  <div className="uk-flex uk-flex-center">
                    <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                      <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid="data-uk-grid">
                        <div className="uk-width-auto">
                          <div style={{
                            width: 235,
                            height: 235
                          }} className="uk-flex uk-flex-center uk-flex-middle">
                            <Link to=""><img className="uk-preserved-width" width="150" height="150" src={data.eml.resourceLogoUrl} alt="" /></Link>
                          </div>
                          <div className="uk-margin-small-top"><Doi label={data.eml.doi} /></div>
                        </div>
                        <div>
                          {data.eml.titleResource	 &&
                            <DataSheet scroll="descripcion" title="Descripción">
                              <div className="uk-padding-small">
                                {data.eml.titleResource	 && <h4 className="uk-text-bold uk-margin-small">Descripción</h4>}
                                {data.eml.titleResource	 && <p>{data.eml.titleResource}</p>}
                                {data.eml_raw["eml:eml"].dataset.project &&
                                  <div>
                                    {data.eml_raw["eml:eml"].dataset.project.studyAreaDescription && data.eml_raw["eml:eml"].dataset.project.studyAreaDescription.descriptor.descriptorValue && <h4 className="uk-text-bold uk-margin-small">Área de estudio</h4>}
                                    {data.eml_raw["eml:eml"].dataset.project.studyAreaDescription && data.eml_raw["eml:eml"].dataset.project.studyAreaDescription.descriptor.descriptorValue !== '' && <p>{data.eml_raw["eml:eml"].dataset.project.studyAreaDescription.descriptor.descriptorValue}</p>}
                                    {data.eml_raw["eml:eml"].dataset.project.funding && data.eml_raw["eml:eml"].dataset.project.funding.para && <h4 className="uk-text-bold uk-margin-small">Financiación</h4>}
                                    {data.eml_raw["eml:eml"].dataset.project.funding && data.eml_raw["eml:eml"].dataset.project.funding.para && <p>{data.eml_raw["eml:eml"].dataset.project.funding.para}</p>}
                                    {data.eml_raw["eml:eml"].dataset.project.designDescription && 
                                      <div>
                                        {data.eml_raw["eml:eml"].dataset.project.designDescription.description.para && <h4 className="uk-text-bold uk-margin-small">Descripción del diseño</h4>}
                                        {data.eml_raw["eml:eml"].dataset.project.designDescription.description.para && <p>{data.eml_raw["eml:eml"].dataset.project.designDescription.description.para}</p>}
                                      </div>
                                    }
                                    {data.eml_raw["eml:eml"].dataset.project.personnel && <h4 className="uk-text-bold uk-margin-small">Personas asociadas al proyecto</h4>}
                                    {data.eml_raw["eml:eml"].dataset.project.personnel && <ContactsGrid data={[data.eml_raw["eml:eml"].dataset.project.personnel]} grid="1" />}
                                  </div>
                                }
                              </div>
                            </DataSheet>
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </ul>
          </div>) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>
        }
      </GenericPage>
    )
  }
}

export default Dataset;
