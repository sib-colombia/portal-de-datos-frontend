import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { map } from 'lodash';
import StackGrid from "react-stack-grid";
//import { union, indexOf, fill } from 'lodash';

import GenericPage from '../../templates/GenericPage';
import Header from '../../organisms/Header';
import Footer from '../../organisms/Footer';
import Details from '../../molecules/Details';
import ExtensionTable from '../../molecules/ExtensionTable';
import HumboldtMap from '../../molecules/HumboldtMap';
import * as OccurrenceService from '../../../services/OccurrenceService';
import Loading from '../../atoms/Loading';
import Title from '../../atoms/Title';

import { Marker } from 'react-leaflet';

class Occurrence extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null
    }
  }

  componentDidMount() {
    if (this.props.match.params.id !== undefined) {
      OccurrenceService.getOccurrence(this.props.match.params.id).then(data => {
        this.setState({ respuesta: data})
      }).catch(err => {
        console.log("ERROR ")
        console.log(err)
        //alert("No se encontró el registro")
        //window.location.href = `/`
      })
    }
  }

  organismoVacio(data) {
    if ((!data.organismQuantity || data.organismQuantity === '') &&
      (!data.organismQuantityType || data.organismQuantityType === '') &&
      (!data.organismName || data.organismName === '') &&
      (!data.organismScope || data.organismScope === '') &&
      (!data.associatedOrganisms || data.associatedOrganisms === '') &&
      (!data.organismRemarks || data.organismRemarks === '') &&
      (!data.previousIdentifications || data.previousIdentifications === '') &&
      (!data.associatedOccurrences || data.associatedOccurrences === ''))
      return true
    return false
  }

  registroVacio(data) {
    if ((!data.occurrenceID || data.occurrenceID === '') &&
      (!data.catalogNumber || data.catalogNumber === '') &&
      (!data.occurrenceRemarks || data.occurrenceRemarks === '') &&
      (!data.recordNumber || data.recordNumber === '') &&
      (!data.recordedBy || data.recordedBy === '') &&
      (!data.organismID || data.organismID === '') &&
      (!data.individualCount || data.individualCount === undefined) &&
      (!data.sex || data.sex === '') &&
      (!data.lifeStage || data.lifeStage === '') &&
      (!data.reproductiveCondition || data.reproductiveCondition === '') &&
      (!data.behavior || data.behavior === '') &&
      (!data.establishmentMeans || data.establishmentMeans === '') &&
      (!data.occurrenceStatus || data.occurrenceStatus === '') &&
      (!data.preparations || data.preparations === '') &&
      (!data.disposition || data.disposition === '') &&
      (!data.otherCatalogNumbers || data.otherCatalogNumbers === '') &&
      (!data.associatedMedia || data.associatedMedia === '') &&
      (!data.associatedReferences || data.associatedReferences === '') &&
      (!data.associatedSequences || data.associatedSequences === '') &&
      (!data.associatedTaxa || data.associatedTaxa === ''))
      return true
    return false
  }
  elementoVacio(data) {
    if ((!data.basisOfRecord || data.basisOfRecord === '') &&
      (!data.institutionCode || data.institutionCode === '') &&
      (!data.collectionCode || data.collectionCode === '') &&
      (!data.type || data.type === '') &&
      (!data.modified || data.modified === '') &&
      (!data.language || data.language === '') &&
      (!data.license || data.license === '') &&
      (!data.rightsHolder || data.rightsHolder === '') &&
      (!data.accessRights || data.accessRights === '') &&
      (!data.bibliographicCitation || data.bibliographicCitation === '') &&
      (!data.references || data.references === '') &&
      (!data.institutionID || data.institutionID === '') &&
      (!data.collectionID || data.collectionID === '') &&
      (!data.datasetID || data.datasetID === '') &&
      (!data.datasetName || data.datasetName === '') &&
      (!data.ownerInstitutionCode || data.ownerInstitutionCode === '') &&
      (!data.informationWithheld || data.informationWithheld === '') &&
      (!data.dataGeneralizations || data.dataGeneralizations === '') &&
      (!data.dynamicProperties || data.dynamicProperties === ''))
      return true
    return false
  }
  eventoVacio(data) {
    if ((!data.eventID || data.eventID === '') &&
      (!data.parentEventID || data.parentEventID === '') &&
      (!data.samplingProtocol || data.samplingProtocol === '') &&
      (!data.sampleSizeValue || data.sampleSizeValue === '') &&
      (!data.sampleSizeUnit || data.sampleSizeUnit === '') &&
      (!data.samplingEffort || data.samplingEffort === '') &&
      (!data.eventDate || data.eventDate === '') &&
      (!data.eventTime || data.eventTime === '') &&
      (!data.startDayOfYear || data.startDayOfYear === '') &&
      (!data.endDayOfYear || data.endDayOfYear === '') &&
      (!data.year || data.year === '') &&
      (!data.month || data.month === '') &&
      (!data.day || data.day === '') &&
      (!data.verbatimEventDate || data.verbatimEventDate === '') &&
      (!data.habitat || data.habitat === '') &&
      (!data.fieldNumber || data.fieldNumber === '') &&
      (!data.fieldNotes || data.fieldNotes === '') &&
      (!data.eventRemarks || data.eventRemarks === ''))
      return true
    return false
  }
  ubicacionVacio(data) {
    if ((!data.locationID || data.locationID === '') &&
      (!data.higherGeographyID || data.higherGeographyID === '') &&
      (!data.higherGeography || data.higherGeography === '') &&
      (!data.continent || data.continent === '') &&
      (!data.waterBody || data.waterBody === '') &&
      (!data.islandGroup || data.islandGroup === '') &&
      (!data.island || data.island === '') &&
      (!data.country || data.country === '') &&
      (!data.countryCode || data.countryCode === '') &&
      (!data.stateProvince || data.stateProvince === '') &&
      (!data.county || data.county === '') &&
      (!data.municipality || data.municipality === '') &&
      (!data.locality || data.locality === '') &&
      (!data.verbatimLocality || data.verbatimLocality === '') &&
      (!data.verbatimElevation || data.verbatimElevation === '') &&
      (!data.minimumElevationInMeters || data.minimumElevationInMeters === '') &&
      (!data.maximumElevationInMeters || data.maximumElevationInMeters === '') &&
      (!data.verbatimDepth || data.verbatimDepth === '') &&
      (!data.minimumDepthInMeters || data.minimumDepthInMeters === '') &&
      (!data.maximumDepthInMeters || data.maximumDepthInMeters === '') &&
      (!data.minimumDistanceAboveSurfaceInMeters || data.minimumDistanceAboveSurfaceInMeters === '') &&
      (!data.maximumDistanceAboveSurfaceInMeters || data.maximumDistanceAboveSurfaceInMeters === '') &&
      (!data.locationAccordingTo || data.locationAccordingTo === '') &&
      (!data.locationRemarks || data.locationRemarks === '') &&
      (!data.verbatimCoordinates || data.verbatimCoordinates === '') &&
      (!data.verbatimLatitude || data.verbatimLatitude === '') &&
      (!data.verbatimLongitude || data.verbatimLongitude === '') &&
      (!data.verbatimCoordinateSystem || data.verbatimCoordinateSystem === '') &&
      (!data.verbatimSRS || data.verbatimSRS === '') &&
      (!data.decimalLatitude || data.decimalLatitude === '') &&
      (!data.decimalLongitude || data.decimalLongitude === '') &&
      (!data.geodeticDatum || data.geodeticDatum === '') &&
      (!data.coordinateUncertaintyInMeters || data.coordinateUncertaintyInMeters === '') &&
      (!data.coordinatePrecision || data.coordinatePrecision === '') &&
      (!data.pointRadiusSpatialFit || data.pointRadiusSpatialFit === '') &&
      (!data.footprintWKT || data.footprintWKT === '') &&
      (!data.footprintSRS || data.footprintSRS === '') &&
      (!data.footprintSpatialFit || data.footprintSpatialFit === '') &&
      (!data.georeferencedBy || data.georeferencedBy === '') &&
      (!data.georeferencedDate || data.georeferencedDate === '') &&
      (!data.georeferenceProtocol || data.georeferenceProtocol === '') &&
      (!data.georeferenceSources || data.georeferenceSources === '') &&
      (!data.georeferenceVerificationStatus || data.georeferenceVerificationStatus === '') &&
      (!data.georeferenceRemarks || data.georeferenceRemarks === ''))
      return true
    return false
  }
  taxonVacio(data) {
    if ((!data.taxonID || data.taxonID === '') &&
      (!data.scientificNameID || data.scientificNameID === '') &&
      (!data.acceptedNameUsageID || data.acceptedNameUsageID === '') &&
      (!data.parentNameUsageID || data.parentNameUsageID === '') &&
      (!data.originalNameUsageID || data.originalNameUsageID === '') &&
      (!data.nameAccordingToID || data.nameAccordingToID === '') &&
      (!data.namePublishedInID || data.namePublishedInID === '') &&
      (!data.taxonConceptID || data.taxonConceptID === '') &&
      (!data.scientificName || data.scientificName === '') &&
      (!data.acceptedNameUsage || data.acceptedNameUsage === '') &&
      (!data.parentNameUsage || data.parentNameUsage === '') &&
      (!data.originalNameUsage || data.originalNameUsage === '') &&
      (!data.nameAccordingTo || data.nameAccordingTo === '') &&
      (!data.namePublishedIn || data.namePublishedIn === '') &&
      (!data.namePublishedInYear || data.namePublishedInYear === '') &&
      (!data.higherClassification || data.higherClassification === '') &&
      (!data.kingdom || data.kingdom === '') &&
      (!data.phylum || data.phylum === '') &&
      (!data.class || data.class === '') &&
      (!data.order || data.order === '') &&
      (!data.family || data.family === '') &&
      (!data.genus || data.genus === '') &&
      (!data.subgenus || data.subgenus === '') &&
      (!data.specificEpithet || data.specificEpithet === '') &&
      (!data.infraspecificEpithet || data.infraspecificEpithet === '') &&
      (!data.taxonRank || data.taxonRank === '') &&
      (!data.verbatimTaxonRank || data.verbatimTaxonRank === '') &&
      (!data.scientificNameAuthorship || data.scientificNameAuthorship === '') &&
      (!data.vernacularName || data.vernacularName === '') &&
      (!data.nomenclaturalCode || data.nomenclaturalCode === '') &&
      (!data.taxonomicStatus || data.taxonomicStatus === '') &&
      (!data.nomenclaturalStatus || data.nomenclaturalStatus === '') &&
      (!data.taxonRemarks || data.taxonRemarks === ''))
      return true
    return false
  }
  identificacionVacio(data) {
    if ((!data.identificationID || data.identificationID === '') &&
      (!data.identifiedBy || data.identifiedBy === '') &&
      (!data.dateIdentified || data.dateIdentified === '') &&
      (!data.identificationReferences || data.identificationReferences === '') &&
      (!data.identificationVerificationStatus || data.identificationVerificationStatus === '') &&
      (!data.identificationRemarks || data.identificationRemarks === '') &&
      (!data.identificationQualifier || data.identificationQualifier === '') &&
      (!data.typeStatus || data.typeStatus === ''))
      return true
    return false
  }
  muestraVacio(data) {
    if ((!data.materialSampleID || data.materialSampleID === ''))
      return true
    return false
  }
  contextoGeologicoVacio(data) {
    if ((!data.geologicalContextID || data.geologicalContextID === '') &&
      (!data.earliestEonOrLowestEonothem || data.earliestEonOrLowestEonothem === '') &&
      (!data.latestEonOrHighestEonothem || data.latestEonOrHighestEonothem === '') &&
      (!data.earliestEraOrLowestErathem || data.earliestEraOrLowestErathem === '') &&
      (!data.latestEraOrHighestErathem || data.latestEraOrHighestErathem === '') &&
      (!data.earliestPeriodOrLowestSystem || data.earliestPeriodOrLowestSystem === '') &&
      (!data.latestPeriodOrHighestSystem || data.latestPeriodOrHighestSystem === '') &&
      (!data.earliestEpochOrLowestSeries || data.earliestEpochOrLowestSeries === '') &&
      (!data.latestEpochOrHighestSeries || data.latestEpochOrHighestSeries === '') &&
      (!data.earliestAgeOrLowestStage || data.earliestAgeOrLowestStage === '') &&
      (!data.latestAgeOrHighestStage || data.latestAgeOrHighestStage === '') &&
      (!data.lowestBiostratigraphicZone || data.lowestBiostratigraphicZone === '') &&
      (!data.highestBiostratigraphicZone || data.highestBiostratigraphicZone === '') &&
      (!data.lithostratigraphicTerms || data.lithostratigraphicTerms === '') &&
      (!data.group || data.group === '') &&
      (!data.formation || data.formation === '') &&
      (!data.member || data.member === '') &&
      (!data.bed || data.bed === ''))
      return true
    return false
  }

  render() {
    // let basicInformation = false
    let o = {}
    // let d = {}
    // let p = {}
    // // let e = {}
    // // let r = {}
    // // let m = {}
    // if (this.state.occurrenceBasic) {
    //   o = this.state.occurrenceBasic.results.occurrence
    //   d = this.state.occurrenceBasic.results.dataset
    //   p = this.state.occurrenceBasic.results.organization
    //   // e = this.state.occurrenceBasic.results.measurementOrFact
    //   // r = this.state.occurrenceBasic.results.resourceRelationship
    //   // m = this.state.occurrenceBasic.results.multimedia

    const { respuesta } = this.state;
    
    if (respuesta===undefined){
      return (
          <GenericPage titlep="Búsqueda - Portal de datos" header={<Header />} footer={<Footer />}>
            <Loading />
          </GenericPage>
        )
    }
    
    let occurrenceBasic = respuesta.occurrence
    let d = respuesta.dataset.eml
    let p = respuesta.organization.provider
    
    o = occurrenceBasic;
    
    if (o){
      console.log("o.taxon_rank")
      console.log(o.taxon_rank)
    }
    
    let basicInformation = occurrenceBasic && {
      scientificName: occurrenceBasic.scientificName,
      breadcrumb: {
        kingdom: occurrenceBasic.kingdom,
        phylum: occurrenceBasic.phylum,
        class: occurrenceBasic.class,
        order: occurrenceBasic.order,
        family: occurrenceBasic.family,
        genus: occurrenceBasic.genus,
        specificEpithet: occurrenceBasic.specificEpithet,
      },
      breadcrumb2: [
        occurrenceBasic.kingdom,
        occurrenceBasic.phylum,
        occurrenceBasic.class,
        occurrenceBasic.order,
        occurrenceBasic.family,
        occurrenceBasic.genus,
        occurrenceBasic.specificEpithet,
      ],
      country: occurrenceBasic.country,
      stateProvince: occurrenceBasic.stateProvince,
      basisOfRecord: occurrenceBasic.basisOfRecord,
      habitat: occurrenceBasic.habitat,
      resourceName: occurrenceBasic.datasetName,
      providerName: occurrenceBasic.provider_name,
      decimalLongitude: occurrenceBasic.decimalLongitude,
      decimalLatitude: occurrenceBasic.decimalLatitude,
      description: occurrenceBasic.abstract
    }
    
    if (basicInformation){
      while (basicInformation.breadcrumb2.slice(-1)[0]===""){
        basicInformation.breadcrumb2.pop()
      }
      basicInformation.breadcrumb2.pop()
    }
    //while(basicInformation.breadcrumb.slice(-1)[0]!==""){
    //}
    

    console.log(o)
    return (
    
      <GenericPage titlep="Búsqueda - Portal de datos" header={<Header />} footer={<Footer />}>
        {
          (occurrenceBasic &&
            <div className="uk-section uk-padding-remove-bottom">
              <div className="uk-flex-center" data-uk-grid>
                <div>
                  <h4 className="uk-heading-divider" style={{ borderBottomColor: '#ff7847' }}>
                    REGISTRO BIOLÓGICO
                    {o.child.event && 
                      <img className="uk-margin-small-left" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/icono-evento.png" title="Evento de muestreo" alt="Evento de muestreo"/>
                    }
                  </h4>
                </div>
              </div>
              <div className="uk-flex-center uk-grid-collapse uk-child-width-1-1" data-uk-grid>
                <div className="uk-width-5-6 uk-width-2-3@m uk-text-center uk-margin-large-top uk-padding-remove">
                  <h2 className="scientificName">{basicInformation.scientificName}</h2>
                </div>
                <div className="uk-flex uk-flex-center uk-margin-small-top">
                  {
                    map(basicInformation.breadcrumb2, (v, k) => {
                      const last = basicInformation.breadcrumb2.length-1;
                      return v && (
                        <div key={k}>
                          <h5 className={k===5?"scientificName":""}>{v}
                            {last !== k && <span data-uk-icon="icon: chevron-right; ratio: 0.85"></span>}
                          </h5>
                        </div>
                      )
                    })
                  }

                </div>
              </div>
              
              <hr className="uk-margin-remove-bottom" />
              <div className="uk-flex uk-flex-center">
                <div className="uk-card uk-card-default uk-text-lead uk-text-center" style={{ paddingTop: 3, paddingBottom: 3, marginTop: -20 }}>
                  <div className="uk-margin-left uk-margin-right uk-text-upper-little">ID DEL REGISTRO BIOLÓGICO</div>
                  <span className="uk-margin-left uk-margin-right uk-text-bold">{o.occurrenceID}</span>
                </div>
              </div>

              <div className="uk-section uk-section-small uk-flex uk-flex-center">
                <div className="uk-width-5-6 uk-width-2-3@m">
                  <ul className="uk-list">
                    <li className="uk-child-width-expand" data-uk-grid>
                      <div className="uk-grid-small" data-uk-grid>
                        <span className="uk-text-bold">Nombre científico:</span>
                        <a className="uk-text-tertiary" href="">{basicInformation.scientificName}</a>
                      </div>
                      <div className="uk-grid-small uk-margin-remove-top uk-child-width-expand" data-uk-grid>
                        <span className="uk-text-bold uk-width-auto">Nombre del recurso:</span>
                        <Link className="uk-text-tertiary" to={`/dataset/`+o.gbifId}>{d.titleResource}</Link>
                      </div>
                    </li>
                    <li className="uk-child-width-expand" data-uk-grid>
                      <div className="uk-grid-small" data-uk-grid>
                        <span className="uk-text-bold">Ubicación:</span>
                        <span>{basicInformation.country}, {basicInformation.stateProvince}</span>
                      </div>
                      <div className="uk-grid-small uk-margin-remove-top uk-child-width-expand" data-uk-grid>
                        <span className="uk-text-bold uk-width-auto">Publicador:</span>
                        {o.organizationId &&
                          <Link
                            className="uk-text-tertiary"
                            to={`/provider/` + o.organizationId}
                          >{p.title}
                          </Link>
                        }
                      </div>
                    </li>
                    <li className="uk-child-width-expand" data-uk-grid>
                      <div className="uk-grid-small" data-uk-grid>
                        <span className="uk-text-bold">Base del Registro:</span>
                        <span>{occurrenceBasic.basisOfRecord}</span>
                      </div>
                      <div className="uk-grid-small" data-uk-grid>
                        {basicInformation.habitat &&
                          <li className="uk-grid-small" data-uk-grid>
                            <span className="uk-text-bold uk-margin-small-right">Hábitat:</span>
                            <span>{basicInformation.habitat}</span>
                          </li>
                        }
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              {
                (o.hasLocation && o.decimalLatitude !== undefined && o.decimalLongitude !== undefined) &&
                <div className="uk-height-large">
                  <HumboldtMap center={[parseInt(occurrenceBasic.location[1], 10), parseInt(occurrenceBasic.location[0], 10)]} zoom={8}>
                    <Marker position={{ lat: parseInt(occurrenceBasic.location[1], 10), lon: parseInt(occurrenceBasic.location[0], 10) }} />
                  </HumboldtMap>
                </div>
              }
              <div className="uk-section uk-section-small uk-flex uk-flex-center">
                <div className="uk-width-5-6 uk-width-2-3@m">
                  <Title label="Sobre el recurso" tag="h3" />
                  <div className="uk-column-1-2 uk-margin-top">
                    <p>{d.abstract}</p>
                  </div>
                </div>
              </div>
              <div className="uk-section uk-section-default uk-section-small uk-flex uk-flex-center">
                <div className="uk-width-5-6 uk-width-2-3@m">
                  <StackGrid columnWidth={"50%"} monitorImagesLoaded gutterWidth={40} gutterHeight={20}>
                    {!this.registroVacio(o) &&
                      <Details title="Registro">
                        <Details.Registro data={o} />
                      </Details>
                    }
                    {!this.elementoVacio(o) &&
                      <Details title="Elemento de registro">
                        <Details.Occurrence data={o} />
                      </Details>
                    }
                    {!this.eventoVacio(o) &&
                      <Details title="Evento">
                        <Details.Event data={o} />
                      </Details>
                    }
                    {!this.ubicacionVacio(o) &&
                      <Details title="Ubicación">
                        <Details.Location data={o} />
                      </Details>
                    }
                    {!this.taxonVacio(o) &&
                      <Details title="Taxón">
                        <Details.Taxon data={o} />
                      </Details>
                    }
                    {!this.identificacionVacio(o) &&
                      <Details title="Identificación">
                        <Details.Identification data={o} />
                      </Details>
                    }
                    {!this.organismoVacio(o) &&
                      <Details title="Organismo">
                        <Details.Organism data={o} />
                      </Details>
                    }
                    {!this.muestraVacio(o) &&
                      <Details title="Muestra">
                        <Details.Sample data={o} />
                      </Details>
                    }
                    {!this.contextoGeologicoVacio(o) &&
                      <Details title="Contexto geológico">
                        <Details.GeologicalContext data={o} />
                      </Details>
                    }
                  </StackGrid>
                </div>
              </div>
              <div className="uk-section uk-section-small" style={{ backgroundColor: '#e5e5e5' }}>
                <div className="uk-container uk-width-5-6@m uk-width-2-3@l">
                  <ExtensionTable titulo="Evento de muestreo" data={o.child.event} />
                  <ExtensionTable titulo="Medidas o hechos" data={o.child.measurementorfact} />
                  <ExtensionTable titulo="Medidas o hechos extendido" data={o.child.extendedmeasurementorfact} />
                  <ExtensionTable titulo="Recursos relacionados" data={o.child.resourcerelationship} />
                  <ExtensionTable titulo="Multimedia" data={o.child.multimedia} />
                  <ExtensionTable titulo="Referencias" data={o.child.reference} />
                  <ExtensionTable titulo="Información de parcelas" data={o.child.releve} />
                </div>
              </div>
            </div>
          ) || <div className="uk-flex uk-flex-center uk-flex-middle uk-padding" data-uk-height-viewport="offset-top: true"><Loading /></div>
        }
      </GenericPage>
    )
  }
}

export default Occurrence;
