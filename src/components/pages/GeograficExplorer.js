import React, { Component } from 'react';
import { map, split, indexOf, join, upperFirst } from 'lodash';
import NumberFormat from 'react-number-format';
//import { Link } from 'react-router-dom';

import LogicPage from '../templates/LogicPage';
import Header from '../organisms/Header';
import Sidebar from '../organisms/Sidebar';
import Footer from '../organisms/Footer';
import LiveMap from '../molecules/LiveMap';
import TableMap from '../molecules/TableMap';
import Filters from '../molecules/Filters';
import Title from '../atoms/Title';
import * as MapService from '../../services/MapService';
import Link from "react-router-dom/es/Link";

class GeograficExplorer extends Component {

  constructor() {
    super();
    this.state = {
      count: null,
      data: null,
      anteriorH: -1,
      anteriorW: -1,
      minHeigth: 450,
      geoJson: [],
      mapa: true
    }

    this.count = []
    this.search = []
  }

  componentDidMount() {
    const search = split((this.props.location.search).slice(1), '&')
    this.fillMapValues(search)
    this.getGeoJson(search)
  }

  fillMapValues(search) {
    this.setState({ data: null }, () => {
      MapService.getOccurrenceList(search)
        .then(data => {
          let d = data.aggregations.filter_agg[2]
          //console.log("data.aggregations.filter_agg")
          //console.log(data.aggregations)
          if (d.buckets.length > 0) {
            let number = d.doc_count
            data = d.buckets
            let markers = []
            map(data, (v, k) => {
              let lat = parseFloat(v[3].location.lat)
              let lon = parseFloat(v[3].location.lon)
              
              //console.log(v[3])
              //console.log(v)

              if (!isNaN(lat) && !isNaN(lon)) {
                let id = v[3].count
                let scientificName = v[3].count
                let GBIFID = v[3].count
                let title_resource = v[3].count
                let provider_id = v[3].count
                let organizationName = v[3].count
                let basisOfRecord  = v[3].count
                let count  = v[3].count
                markers[k] = {
                  position: [parseFloat(lat), parseFloat(lon)],
                  data: { id, scientificName, GBIFID, title_resource, provider_id, organizationName, basisOfRecord, count }
                }
              }
            })
            this.setState({ data: markers, number })
          } else {
            //alert("No se encontraron puntos")
          }
        })
    })
  }

  setCount(e, filter) {
    this.count[filter] = e.length;
    this.search[filter] = e;
    const stringQ = this.createQuery(this.search);
    window.history.replaceState('', 'Búsqueda - Portal de datos', `${this.props.location.pathname}?${stringQ}`)

    this.setState({ geoJson: [] }, () => this.getGeoJson(split(stringQ, '&')))
    this.props.location.search = ''
    this.setState({ count: this.count })
  }

  createQuery(data) {
    let search = this.props.location.search !== '' ? split((this.props.location.search).slice(1), '&') : [];
    map(data, (value, key) => {
      map(value, (value1) => {
        const i = indexOf(search, encodeURI(value1.id));
        if (i < 0) {
          search.push(value1.id)
        }
      })
    })
    this.fillMapValues(search)
    return join(search, '&');
  }

  activeFilters() {
    const search = this.props.location.search !== '' && split((this.props.location.search).slice(1), '&');
    return search;
  }

  getGeoJson(search) {
    let state = [];
    map(search, (v, k) => {
      let split = (decodeURI(v)).split('=')
      let a = split.pop().toLowerCase().trim()
      if (v.match(/^geo_departments.*$/)) {
        state.push({ v: upperFirst(a), path: 'Departamentos' })
      } else if (v.match(/^geo_minicipalities.*$/)) {
        state.push({ v: upperFirst(a), path: 'Municipios' })
      } else if (v.match(/^geo_natural_regions.*$/)) {
        state.push({ v: upperFirst(a), path: 'RegionesNaturales' })
      } else if (v.match(/^geo_corporations.*$/)) {
        state.push({ v: upperFirst(a), path: 'CorporacionesAutonomasRegionales' })
      } else if (v.match(/^ecosistemaestrategico.*$/)) {
        state.push({ v: upperFirst(a), path: 'EcosistemasEstrategico' })
      } else if (v.match(/^areaprotegida.*$/)) {
        state.push({ v: upperFirst(a), path: 'AreaProtegida' })
      } else if (v.match(/^geo_paramo.*$/)) {
        state.push({ v: upperFirst(a), path: 'EcosistemasEstrategicos/Paramo' })
      } else if (v.match(/^geo_tropical_dry_forest.*$/)) {
        state.push({ v: upperFirst(a), path: 'EcosistemasEstrategicos/BosqueSecoTropical' })
      } else if (v.match(/^geo_natural_national_parks.*$/)) {
        state.push({ v: upperFirst(a), path: 'AreasProtegidas/ParquesNacionalesNaturales' })
      } else if (v.match(/^geo_natural_reserves_civil_society.*$/)) {
        state.push({ v: upperFirst(a), path: 'AreasProtegidas/ReservasNaturalesDeLaSociedadCivil' })
      } else if (v.match(/^geo_other_protected_areas.*$/)) {
        state.push({ v: upperFirst(a), path: 'AreasProtegidas/OtrasAreasProtegidas' })
      } else if (v.match(/^geo_colombia.*$/)) {
        state.push({ v: upperFirst(a), path: 'Colombia' })
      }
    })

    if (state.length > 0) {
      map(state, ({ v, path }) => {
        MapService.getGeoJson(v, path).then(data => {
          this.setState((prevState) => {
            if (prevState.geoJson[v] === undefined)
              prevState.geoJson[v] = data

            return { geoJson: prevState.geoJson }
          })
        })
      })
    } else {
      this.setState({ geoJson: [] })
    }
  }

  openPopup(data) {
    if (data)
      this.setState({ popup: null }, () => {

        let scientificName = split(data.scientificName, ' ')
        //let name, epithet;
        //name = scientificName[0] + " " + scientificName[1]
        scientificName.shift()
        scientificName.shift()
        //epithet = scientificName.join(" ")
        
        let xrecurso = data.title_resource
        //let xproveedor = data.organizationName
        //let xbase = data.basisOfRecord
                

/*

                  <Link to={`/occurrence/${data.id}`} className="uk-link-reset uk-flex uk-flex-column">
                    <i style={{ fontSize: 17 }}>{name}</i>
                    <span className="uk-text-small">{epithet}</span>
                  </Link>

                <div className="uk-flex uk-flex-column uk-text-small"><span className="uk-text-bold uk-margin-small-right">RECURSO</span><Link to={`/dataset/${data.GBIFID}`} className="uk-link-muted uk-text-truncate">{xrecurso}</Link></div>
                <div className="uk-flex uk-flex-column uk-text-small"><span className="uk-text-bold uk-margin-small-right">PUBLICADOR</span><Link to={`/provider/${data.provider_id}`} className="uk-link-muted  uk-text-truncate">{xproveedor}</Link></div>
                <div className="uk-flex uk-flex-column uk-text-small uk-text-truncate"><span className="uk-text-bold uk-margin-small-right">BASE DEL REGISTRO</span>{xbase}</div>
*/
        this.setState({
          popup: (
            <div className="uk-position-bottom uk-overlay uk-overlay-primary uk-padding-small" style={{ zIndex: 979 }}>
              <div className="uk-grid-small uk-grid-divider uk-flex-middle uk-child-width-1-1" data-uk-grid>
                <div className="uk-flex uk-flex-middle">
                  <span data-uk-icon="icon: location; ratio: 1.5" className="uk-margin-small-left uk-margin-right" />
                  {xrecurso} registros en este lugar.
                </div>
              </div>
            </div>
          )
        })
      }
      )
  }
  
  ver(que){
    if (que==="tabla"){
      this.setState({mapa: false})
    }else{
      this.setState({mapa: true})
    }
  }

  render() {
/*
          <div className="uk-grid-collapse uk-child-width-expand uk-flex-bottom" data-uk-grid>
            <div className="uk-width-auto">
              <ul className="uk-margin-medium-top" data-uk-tab="swiping: false" >
                <li>
                  <a className="uk-text-right"><span className="uk-text-top" data-uk-icon="icon: download"></span> Descarga</a>
                  <div className="uk-padding-small" data-uk-dropdown="mode: hover">
                    <ul className="uk-nav uk-dropdown-nav">
                      <li><a>Próximamente</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
            <div>
              <ul className="uk-margin-medium-top" data-uk-tab="swiping: false" data-uk-switcher={`connect: .switcher-search; active: ${this.state.tab}; swiping: false`}>
                {this.state.mapa && <li className=""><a onClick={() => this.ver('tabla')} ><span className="uk-text-primary uk-text-top" uk-icon="icon: triangle-right" />VER EN TABLA</a></li>}
                {!this.state.mapa && <li className=""><a onClick={() => this.ver('mapa')} ><span className="uk-text-primary uk-text-top" uk-icon="icon: triangle-right" />OCULTAR TABLA</a></li>}
              </ul>
            </div>

*/  
  
    return (
      <LogicPage
        titlep="Búsqueda - Portal de datos"
        /*headercustom={<Header withSidebar={Sidebar.id} */
        headercustom={<Header withSidebar={false}
        />}
        sidebar={
          <Filters count={this.state.count}>
            <Filters.Taxonomy activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 0)} />
            <Filters.GeoPais activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 1)} />
            <Filters.GeoDepartamento activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />
            <Filters.GeoMunicipio activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 3)} />
            <Filters.StrategicEcosystems activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
            <Filters.GeoRegiones activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 5)} />
            <Filters.ProtectedAreas activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 6)} />
            <Filters.Cars activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 8)} />
            {/*<Filters.SearchMap activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 6)} />*/}
          </Filters>
        }
        footer={<Footer />}
      >
        <div className="uk-section uk-padding-remove uk-margin-medium-top uk-margin">
          <Title
            label={<span className="uk-text-bold">EXPLORADOR GEOGRÁFICO</span>}
            icon={<span className="uk-text-primary" uk-icon="icon: triangle-right; ratio: 1.3" />}
            number={<NumberFormat value={this.state.number} displayType="text" thousandSeparator />}
            labelNumber="REGISTROS CON COORDENADAS"
            tag="h4"
          />
        </div>
        {false && !this.state.mapa && this.state.data &&
          <div className="uk-inline uk-heigth-1-1 uk-width-1-1 uk-box-shadow-small">
            <TableMap data={this.state.data.slice(0,100)}  />
          </div>
        }
        <div className="uk-inline uk-heigth-1-1 uk-width-1-1 uk-box-shadow-small">
            <LiveMap geoJson={this.state.geoJson} data={this.state.data} popup={(data) => this.openPopup(data)} />
            {!this.state.data && <div className="uk-position-cover uk-overlay  uk-flex uk-flex-center uk-flex-middle" style={{ zIndex: 979 }}><div uk-spinner="ratio: 2" style={{ color: '#ff7847' }}></div></div>}
            {this.state.popup && this.state.popup}
        </div>
        <div className="overlay">
          {/*<img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-25.png"/><br/>*/}
          <h2 className="no">Estamos mejorando la experiencia del explorador geográfico. ¡Espéralo muy pronto! </h2>
          <div className="linea uk-container-center uk-width-1-4 uk-width-small@s uk-background-primary" style={{height: "2px", }}></div>
          <br/>
          <Link className="uk-button uk-button-primary uk-text-bold" to="/" >Volver al inicio</Link>
        </div>
      </LogicPage>
    )
  }
}

export default GeograficExplorer;
