import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import cx from 'classnames';
import { map, split, indexOf, join } from 'lodash';

import LogicPage from '../templates/LogicPage';
import Header from '../organisms/Header';
import Sidebar from '../organisms/Sidebar';
import Footer from '../organisms/Footer';
import Title from '../atoms/Title';
import DatasetsList from '../organisms/DatasetsList';
import DatasetsGrid from '../organisms/DatasetsGrid';
import Pagination from '../atoms/Pagination';
import * as DatasetService from '../../services/DatasetService';
import Filters from '../molecules/Filters';
import DatasetsTable from "../organisms/DatasetsTable";

class Datasets extends Component {

  constructor(props) {
    super(props);
    this.state = {
      kind: 0,
      data: null,
      count: '',
      type: "ALL",
      amount: null,
      tab: 0,
      search: props.location.search
    }

    this.count = [];
    this.search = [];
  }

  componentDidMount() {
    this.offsetResults(0, 0,this.state.search)
    this.setState({ modified: true })
  }

  onChangePage(pageOfItems) {
    const page = pageOfItems * 20;
    this.offsetResults(page, pageOfItems, this.state.search);
  }

  offsetResults(offset, pageOfItems, search) {
    DatasetService.getDatasetGrid(offset, search, this.state.orderBy)
      .then(data => {
        this.setState({
        data: data.aggregations.gbifId.buckets,
        count: data.aggregations.gbifId.doc_count_error_upper_bound,
        })
      })
      .catch(() => {
        this.setState({
          data: [],
          count: 0,
        })
      })
  }

  handleDisplay(kind) {this.setState({ kind })}

  cambiarTipo(tipo, num) {
    this.setState({ data: null, type: tipo, tab: num }, () => {
      this.offsetResults(0, 0, this.state.search)
    })
  }

  setCount(e, filter) {
    this.count[filter] = e.length;
    this.search[filter] = e;
    const stringQ = this.createQuery(this.search);
    window.history.replaceState('', 'Búsqueda - Portal de datos', `${this.props.location.pathname}${stringQ && '?' + stringQ}`)
    this.offsetResults(0, 0, stringQ)

    this.setState({ amount: this.count, search: stringQ })
  }

  createQuery(data) {
    let search = this.props.location.search !== '' ? split((this.props.location.search).slice(1), '&') : [];
    search['query'] = []
    map(data, (value, key) => {
      map(value, (value1) => {
        const i = indexOf(search['query'], value1.id);
        if (i < 0) {
          search['query'].push(value1.id)
        }
      })
    })

    if (!this.state.modified && search['query'].length < 0)
      search['query'].unshift(join(search, '&'))

    return join(search['query'], '&');
  }

  activeFilters() {
    return this.props.location.search !== '' && split((this.props.location.search).slice(1), '&');
  }

  orderBy(orderBy) {
    this.setState({ orderBy }, () => this.offsetResults(0))
  }

  render() {
            //<Filters.EventDate activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
    return (
      <LogicPage
        titlep="Recursos - Portal de datos"
        headercustom={<Header withSidebar={Sidebar.id} />}
        sidebar={
          <Filters count={this.state.amount} search={this.state.search}>
            <Filters.ResourceName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 0)} />
            <Filters.ProviderName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 1)} />
            <Filters.Project activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />

            {/*<Filters.Doi activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
            <Filters.License activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 3)} />*/}
          </Filters>
        }
        footer={<Footer />}
      >
        <div className="uk-section uk-section-small">
          <div className="uk-container uk-width-5-6@l uk-width-2-3@xl">
            <Title
              label={<span className="uk-text-bold">BÚSQUEDA POR RECURSO</span>}
              icon={<span className="uk-text-primary" uk-icon="icon: triangle-right; ratio: 1.3" />}
              number={<NumberFormat value={this.state.count} displayType="text" thousandSeparator />}
              labelNumber="RESULTADOS"
              tag="h4"

            />
            <div className="uk-grid-collapse uk-child-width-expand uk-margin uk-flex uk-flex-bottom" data-uk-grid>
              {/*<div>*/}
                {/*<ul className="uk-margin-medium-top" data-uk-tab="swiping: false">*/}
                  {/*<li className={cx({ 'uk-active': this.state.tab === 0 })}><a onClick={() => this.cambiarTipo("ALL", 0)}>TODOS</a></li>*/}
                  {/*<li className={cx({ 'uk-active': this.state.tab === 1 })}><a onClick={() => this.cambiarTipo("OCCURRENCE", 1)}>REGISTROS</a></li>*/}
                  {/*<li className={cx({ 'uk-active': this.state.tab === 2 })}><a onClick={() => this.cambiarTipo("SAMPLING_EVENT", 2)}>EVENTOS DE MUESTREO</a></li>*/}
                  {/*<li className={cx({ 'uk-active': this.state.tab === 3 })}><a onClick={() => this.cambiarTipo("METADATA", 3)}>METADATOS</a></li>*/}
                {/*</ul>*/}
              {/*</div>*/}
              {/*<div className="uk-width-auto">*/}
                <ul className="uk-margin-medium-top uk-flex-right" data-uk-tab="swiping: false;" data-uk-switcher>
                  <li className={cx({ 'uk-active': !this.state.kind })} onClick={() => this.handleDisplay(false)}><a title="Modo lista" data-uk-tooltip><span uk-icon="icon: list"></span></a></li>
                  <li className={cx({ 'uk-active': this.state.kind })} onClick={() => this.handleDisplay(true)}><a title="Modo grilla" data-uk-tooltip><span uk-icon="icon: grid"></span></a></li>
                  {/*<li>
                    <a className="uk-text-right" title="Ordenar" data-uk-tooltip>A/Z</a>
                    <div className="uk-padding-small" data-uk-dropdown="mode: hover">
                      <ul className="uk-nav uk-dropdown-nav">
                        <li onClick={() => this.orderBy('alpha')}><a>Alfabético</a></li>
                        <li onClick={() => this.orderBy('recent')}><a>Recientes</a></li>
                        <li onClick={() => this.orderBy('NRecords')}><a>Número de registros</a></li>
                      </ul>
                    </div>
                  </li>*/}
                </ul>
              {/*</div>*/}
            </div>

            {!this.state.kind ? <DatasetsList data={this.state.data} /> : <DatasetsGrid data={this.state.data} />}
          </div>
        </div>
        {/*{this.state.data && <Pagination items={this.state.count} pageSize={5} onChangePage={(number) => {*/}
          {/*this.onChangePage(number - 1);*/}
        {/*}} />}*/}
      </LogicPage>
    )
  }
}


export default Datasets;
