import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import { map, split, indexOf, join } from 'lodash';
import UIkit from 'uikit'

import LogicPage from '../templates/LogicPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';
import Sidebar from '../organisms/Sidebar';
import Title from '../atoms/Title';
import OccurrencesTable from '../organisms/OccurrencesTable';
import SpeciesTable from '../organisms/SpeciesTable';
import DatasetsTable from '../organisms/DatasetsTable';
import ProvidersSearchTable from '../organisms/ProvidersSearchTable';
import SearchMap from '../organisms/SearchMap';
import Filters from '../molecules/Filters';
import { isAuthenticated } from '../../auth'
import * as OccurrenceService from '../../services/OccurrenceService';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: '',
      count: null,
      search: '',
      modified: false
    }

    this.count = [];
    this.search = [];
    this.child = [];
  }

  componentDidMount() {
    this.activeTab(this.props.match.params.tab, this.props.location.search)
    this.setState({ modified: true })
  }

  componentDidUpdate() {
  }

  activeTab(tab, location) {
    let tabIndex, url;
    switch (tab) {
      /*case 'map':
        tabIndex = 1;
        url = '/search/map';
        if (window.location.href !== window.location.origin + '/search/map' + this.props.location.search)
          window.location.href = '/search/map' + this.props.location.search;

        this.setState({ number: this.state.occurrences })
        break;*/

      case 'species':
        tabIndex = 1;
        url = '/search/species';
        this.setState({ number: this.state.species });
        break;

      case 'datasets':
        tabIndex = 2;
        url = '/search/datasets';
        this.setState({ number: this.state.datasets });
        break;

      case 'providers':
        tabIndex = 3;
        url = '/search/providers';
        this.setState({ number: this.state.providers });
        break;

      default:
        tabIndex = 0;
        url = '/search';
        this.setState({ number: this.state.occurrences })
        break;
    }

    if (location === undefined || (location !== undefined && location.indexOf('?') === 0)) {
      if (this.state.search !== '')
        window.history.replaceState({}, 'Búsqueda - Portal de datos', url + '?' + this.state.search)
      else
        window.history.replaceState({}, 'Búsqueda - Portal de datos', url + this.props.location.search)
    } else {
      window.history.replaceState({}, 'Búsqueda - Portal de datos', url + this.state.search)
    }

    this.setState({ tab: tabIndex });
  }

  setCount(e, filter) {

    this.count[filter] = e.length;
    this.search[filter] = e;
    let stringQ = this.createQuery(this.search);
    window.history.replaceState('', 'Búsqueda - Portal de datos', `${this.props.location.pathname}${stringQ && '?' + stringQ}`)
    map(this.child, (value, key) => {
      value.offsetResults(0, 0, stringQ)
    })

    this.setState({ count: this.count, search: stringQ})
  }

  createQuery(data) {
    let search = this.props.location.search !== '' ? split((this.props.location.search).slice(1), '&') : [];
    search['query'] = []
    map(data, (value, key) => {
      map(value, (value1) => {
        const i = indexOf(search['query'], value1.id);
        if (i < 0) {
          search['query'].push(value1.id)
        }
      })
      console.log("esta es la búsqueda " + search)
    })

    if (!this.state.modified && search['query'].length < 0)
      search['query'].unshift(join(search, '&'))

    return join(search['query'], '&');
  }

  activeFilters() {
    return this.props.location.search !== '' && split((this.props.location.search).slice(1), '&');
  }

  download() {
    let u = isAuthenticated()
    if (!u)
      UIkit.drop('#drop-login') && UIkit.drop('#drop-login').show()
    else {
      let stringQ = this.state.search
      if (stringQ === "") {
        stringQ = this.props.location.search
      }
      console.log("La busqueda actua es: ", stringQ)
      OccurrenceService.descargar(stringQ, u.email,window.location.href).then(data => {
        console.log(data)
        if (data.status === "ok") {
          alert('La descarga sera comprimida y enviada a su correo')
        } else {
          alert('Ocurrió un error con la descarga, por favor contactar con el SIB Colombia')
        }
      });
    }
  }

  render() {

    return (
      <LogicPage
        titlep="Búsqueda - Portal de datos"ovh4
        headercustom={<Header withSidebar={Sidebar.id} search={this.props.location.search} location={this.props.location.pathname} />}
        footer={<Footer />}
        sidebar={
          <Filters count={this.state.count} search={this.state.search}>
            <Filters.Taxonomy activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 0)} />
            <Filters.TaxonRank activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 1)} />
            <Filters.Location activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 2)} />
            <Filters.Habitat activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 3)} />
            <Filters.BasisOfRecord activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
            <Filters.RecordedBy activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 5)} />
            <Filters.ProviderName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 7)} />
            <Filters.Project activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 8)} />
            <Filters.ResourceName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 9)} />
            <Filters.Multimedia activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 10)} />
            <Filters.License activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 11)} />

            {/*
            <Filters.License activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 11)} />
            <Filters.License activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 11)} />
            <Filters.OccurrenceId activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 6)} />
            <Filters.Elevation activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 4)} />
            <Filters.Depth activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 5)} />
            <Filters.BasisOfRecord activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 6)} />
            <Filters.EventDate activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 7)} />
            <Filters.RecordedBy activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 8)} />
            <Filters.OccurrenceId activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 9)} />
            <Filters.ProviderName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 10)} />
            <Filters.Project activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 11)} />
            <Filters.ResourceName activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 12)} />
            <Filters.Multimedia activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 13)} />
            <Filters.License activeFilters={this.activeFilters()} count={(e) => this.setCount(e, 14)} />
            */}
          </Filters>
        }
      >
        <div className="uk-section uk-section-small">
          <div className="uk-container uk-container-expand">
            <Title
              label={<span className="uk-text-bold">BÚSQUEDA POR REGISTROS BIOLÓGICOS</span>}
              icon={<span className="uk-text-primary" uk-icon="icon: triangle-right; ratio: 1.3" />}
              number={<NumberFormat value={this.state.number} displayType="text" thousandSeparator />}
              labelNumber="RESULTADOS"
              tag="h4"
            />
            <div className="uk-grid-collapse uk-child-width-expand uk-margin uk-flex uk-flex-bottom" data-uk-grid>
              <div>
                <ul className="uk-margin-medium-top" data-uk-tab="swiping: false" data-uk-switcher={`connect: .switcher-search; active: ${this.state.tab}; swiping: false`}>
                  <li onClick={() => this.activeTab('')}><a>Tabla</a></li>
                  {/*<li onClick={() => this.activeTab('map')}><a>Mapa</a></li>*/}
                  {<li onClick={() => this.activeTab('species')}><a>Especies</a></li>}
                  <li onClick={() => this.activeTab('datasets')}><a>Recursos</a></li>
                  <li onClick={() => this.activeTab('providers')}><a>Publicadores</a></li>
                </ul>
              </div>
              {
                <div className="uk-width-auto">
                  <ul className="uk-margin-medium-top" data-uk-tab="swiping: false" >
                    <li><a className="uk-text-right" onClick={() => this.download()}><span className="uk-text-top" data-uk-icon="icon: download"></span> Descarga</a></li>
                  </ul>
                </div>
              }
            </div>
            <ul className="uk-switcher switcher-search">
              <OccurrencesTable
                onRef={ref => { this.child[0] = ref }}
                occurrences={(number) => { this.state.tab === 0 ? this.setState({ occurrences: number, number }) : this.setState({ occurrences: number }) }}
                search={this.props.location.search}
              />
              {/*<SearchMap
                onRef={ref => { this.child[1] = ref }}
                search={this.props.location.search}
              />*/}
              {<SpeciesTable
                onRef={ref => { this.child[1] = ref }}
                species={(number) => { this.state.tab === 2 ? this.setState({ species: number, number }) : this.setState({ species: number }) }}
                search={this.props.location.search}
                counter={this.state.number}
              />}

              <DatasetsTable
                onRef={ref => { this.child[2] = ref }}
                datasets={(number) => { this.state.tab === 3 ? this.setState({ datasets: number, number }) : this.setState({ datasets: number }) }}
                search={this.props.location.search}
              />
              <ProvidersSearchTable
                onRef={ref => { this.child[3] = ref }}
                providers={(number) => { this.state.tab === 4 ? this.setState({ providers: number, number }) : this.setState({ providers: number }) }}
                search={this.props.location.search}
              />
            </ul>
          </div>
        </div>
      </LogicPage>
    )
  }
}

export default Search;
