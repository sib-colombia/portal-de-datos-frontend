import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import GenericPage from '../templates/GenericPage';
import Header from '../organisms/Header';
import Footer from '../organisms/Footer';
import GeneralStatisticsCont from '../atoms/GeneralStatisticsCont';
import Title from '../atoms/Title';

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      height: NaN,
      hover1: true,
      hover2: true,
      hover3: true,
      hover4: true,
      hover5: true,
    }

    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {
    this.updateDimensions()
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }

  updateDimensions() {
    this.setState({
      height: (window.innerHeight / 2)
    })
  }
  
  mouseOver(id) {
      let s = this.state
      s["hover"+id] = false
      this.setState(s);
  }
  mouseOut(id) {
      let s = this.state
      s["hover"+id] = true
      this.setState(s);
  }

  render() {
  /*
              <div className="uk-overlay uk-overlay-primary uk-position-top-right uk-position-small uk-padding-small uk-text-center uk-light uk-visible@s">
                <p className="uk-text-small">Ejemplo Copyright &copy;</p>
              </div>
              */
    return (
      <GenericPage titlep="Portal de datos" header={<Header location={this.props.location.pathname} />} footer={<Footer />}>
        <div className="uk-position-relative uk-light" data-uk-slideshow={`min-height: ${this.state.height}; max-height: ${this.state.height}`}>
          <ul className="uk-slideshow-items">
            <li>
              <img src="images/slideshow.jpg" alt="" data-uk-cover="data-uk-cover" />
            </li>
          </ul>
          <div className="uk-position-center uk-position-medium">
            <div className="uk-flex uk-flex-column">
              <Link to="/search" style={{ textDecoration: 'none' }}>
                <div
                  className="uk-overlay uk-padding-small uk-text-center"
                  style={{
                    border: 'solid 1.5px #fff',
                    cursor: 'pointer',
                    backgroundColor: (
                      !this.state.hover
                        ? 'rgba(0,0,0,0.2)'
                        : 'rgba(255,255,255,0.2)'),
                    transition: 'background-color 0.3s ease'
                  }}
                  onMouseEnter={() => this.setState({ hover: true })}
                  onMouseLeave={() => this.setState({ hover: false })}
                >
                  <h3 className="uk-text-bold">EXPLORA LOS DATOS</h3>
                </div>
              </Link>
              <div className="uk-grid-small" data-uk-grid>
                <div><Link to="/datasets" className="uk-text-small uk-text-bold"><span className="uk-text-top" uk-icon="icon: triangle-right; ratio: 1.25" />RECURSOS</Link></div>
                <div><Link to="/providers" className="uk-text-small uk-text-bold"><span className="uk-text-top" uk-icon="icon: triangle-right; ratio: 1.25" />PUBLICADORES</Link></div>
                {/*<div><Link to="/geografic/explorer" className="uk-text-small uk-text-bold"><span className="uk-text-top" uk-icon="icon: triangle-right; ratio: 1.25" />EXPLORADOR GEOGRÁFICO</Link></div>*/}
              </div>
            </div>
          </div>
        </div>
        <div className="uk-flex uk-flex-center uk-margin-left uk-margin-right" style={{ marginTop: -45 }}>
          <div className="uk-width-5-6 uk-width-2-3@m"><GeneralStatisticsCont /></div>
        </div>
        <div className="uk-section uk-flex uk-flex-center">
          <div className="uk-width-5-6 uk-width-2-3@m">
            <Title label="Explora a través de grupos biológicos" tag="h2" />
            <div className="uk-flex-center uk-margin-medium-top uk-child-width-1 uk-child-width-1-2@s uk-child-width-auto@m uk-text-center" data-uk-grid>
              <Link className="uk-link-reset" to="/search?phylum=Acanthocephala&phylum=annelida&phylum=arthropoda&phylum=brachiopoda&phylum=bryozoa&phylum=chaetognatha&phylum=cnidaria&phylum=echinodermata&phylum=gastrotricha&phylum=mollusca&phylum=myxozoa&phylum=nematoda&phylum=onychophora&phylum=platyhelminthes&phylum=porifera&phylum=rotifera&phylum=sipuncula">
                <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-invertebrados.png" alt="" />
                <p className="uk-text-center">INVERTEBRADOS</p>
              </Link>
              <Link className="uk-link-reset" to="/search?phylum=Chordata">
                <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-vertebrados.png" alt="" />
                <p className="uk-text-center">VERTEBRADOS</p>
              </Link>
              <Link className="uk-link-reset" to="/search?kingdom=Plantae">
                <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-plantas.png" alt="" />
                <p className="uk-text-center">PLANTAS</p>
              </Link>
              <Link className="uk-link-reset" to="/search?kingdom=fungi">
                <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-hongos.png" alt="" />
                <p className="uk-text-center">HONGOS</p>
              </Link>
              <Link className="uk-link-reset" to="/search?kingdom=chromista">
                <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-chromista.png" alt="" />
                <p className="uk-text-center">CHROMISTA</p>
              </Link>
              <Link className="uk-link-reset" to="/search?kingdom=archaea&kingdom=bacteria&kingdom=incertae sedis&kingdom=protozoa&kingdom=viruses">
                <img className="uk-card uk-card-default uk-card-body uk-border-circle uk-padding-remove" src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/h-portal-otros.png" alt="" />
                <p className="uk-text-center">OTROS</p>
              </Link>
            </div>
          </div>
        </div>
        <div className="uk-section uk-section-default uk-flex uk-flex-center ">
          <div className="uk-width-5-6 uk-width-2-3@m">
            <div className="uk-flex-around uk-child-width-1 uk-child-width-auto@s" data-uk-grid>
              <Link className="uk-link-reset uk-text-center" to="/search">
                <h2>Registros</h2>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                <img src="/images/registros.png" alt="" width="130" />
                <p className="uk-text-reset">
                  <i>Explora a través del buscador
                  <br />
                    de registros biológicos</i>
                </p>
              </Link>
              <Link className="uk-link-reset uk-text-center" to="/providers">
                <h2>Publicadores</h2>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                <img src="/images/publicadores.png" alt="" width="130" />
                <p className="uk-text-reset">
                  <i>Conoce las entidades que comparten
                  <br />
                    datos a través del SiB Colombia</i>
                </p>
              </Link>
              <Link className="uk-link-reset uk-text-center" to="/datasets">
                <h2>Recursos</h2>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                <img src="/images/recursos.png" alt="" width="130" />
                <p className="uk-text-reset">
                  <i>Explora a través de los recursos
                  <br />
                    disponibles en el portal</i>
                </p>
              </Link>
            </div>
          </div>
        </div>
        {/*<div className="uk-section uk-flex uk-flex-center">
          <div className="uk-width-5-6 uk-width-2-3@m">
            <Title label="Explorador geográfico" tag="h2" />
            <div className="uk-flex-center uk-margin-medium-top uk-child-width-1 uk-child-width-1-3@s uk-child-width-1-3@m uk-child-width-auto@l uk-grid-large" data-uk-grid>
              <Link className="uk-link-reset uk-text-center" to="/geografic/explorer" onMouseOver={()=>{this.mouseOver(1)}} onMouseOut={()=>{this.mouseOut(1)}}>
                <h4 className="uk-margin-top">DEPARTAMENTOS</h4>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                {  this.state["hover1"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-23.png" alt="" width="130" /> }
                { !this.state["hover1"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-18.png" alt="" width="130" /> }
              </Link>
              <Link className="uk-link-reset uk-text-center" to="/geografic/explorer" onMouseOver={()=>{this.mouseOver(2)}} onMouseOut={()=>{this.mouseOut(2)}}>
                <h4 className="uk-margin-top">REGIONES</h4>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                {  this.state["hover2"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-24.png" alt="" width="130" /> }
                { !this.state["hover2"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-19.png" alt="" width="130" /> }
              </Link>
              <Link className="uk-link-reset uk-text-center" to="/geografic/explorer" onMouseOver={()=>{this.mouseOver(3)}} onMouseOut={()=>{this.mouseOut(3)}}>
                <h4 className="uk-margin-top">ECOSISTEMAS</h4>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                {  this.state["hover3"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-25.png" alt="" width="130" /> }
                { !this.state["hover3"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-20.png" alt="" width="130" /> }
              </Link>
              <Link className="uk-link-reset uk-text-center" to="/geografic/explorer" onMouseOver={()=>{this.mouseOver(4)}} onMouseOut={()=>{this.mouseOut(4)}}>
                <h4 className="uk-margin-top">ÁREAS PROTEGIDAS</h4>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                {  this.state["hover4"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-26.png" alt="" width="130" /> }
                { !this.state["hover4"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-21.png" alt="" width="130" /> }
              </Link>
              <Link className="uk-link-reset uk-text-center" to="/geografic/explorer" onMouseOver={()=>{this.mouseOver(5)}} onMouseOut={()=>{this.mouseOut(5)}}>
                <h4 className="uk-margin-top">CARS</h4>
                <div className="uk-flex uk-flex-center uk-margin">
                  <div className="uk-width-small uk-background-primary" style={{ height: 2 }} />
                </div>
                {  this.state["hover5"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-27.png" alt="" width="130" /> }
                { !this.state["hover5"] && <img src="https://statics.sibcolombia.net/sib-resources/images/portal-de-datos/png/mapa-inicio-expgeo-22.png" alt="" width="130" /> }
              </Link>
            </div>
            <Link className="uk-link-reset uk-text-center" to="/geografic/explorer" onMouseOver={()=>{this.mouseOver(5)}} onMouseOut={()=>{this.mouseOut(5)}}>
              <div className="uk-margin-top uk-margin-botton"><span className="uk-text-primary uk-margin-small-left uk-icon" uk-icon="triangle-right" />Explora los datos disponibles a través de múltiples capas geográficas<span className="uk-text-primary uk-margin-small-right uk-icon" uk-icon="triangle-left" /></div>
              <div className="uk-margin-top uk-margin-botton"><button className="uk-button uk-button-default">IR AL EXPLORADOR</button></div>
            </Link>

          </div>
        </div>*/}
        <div className="uk-section uk-light uk-flex uk-flex-center" style={{ backgroundColor: '#00a8b4' }}>
          <div className="uk-width-5-6 uk-width-2-3@m">
            <div className="uk-flex-around uk-flex-middle uk-child-width-1 uk-child-width-expand@s" data-uk-grid>
              <div className="uk-text-center">
                <h1 className="uk-margin-remove">¿Quieres compartir tus datos?</h1>
              </div>
              <div className="uk-width-2-5@s">
                <Link to="/static/sobre_el_portal" style={{ textDecoration: 'none' }}>
                  <div
                    className="uk-overlay uk-padding-small uk-text-center"
                    style={{
                      border: 'solid 1.5px #fff',
                      cursor: 'pointer',
                      backgroundColor: (
                        !this.state.hover
                          ? 'rgba(0,0,0,0)'
                          : 'rgba(255,255,255,0.2)'),
                      transition: 'background-color 0.3s ease'
                    }}
                    onMouseEnter={() => this.setState({ hover: true })}
                    onMouseLeave={() => this.setState({ hover: false })}
                  >
                    <h2 className="uk-margin-remove">ENTÉRATE CÓMO</h2>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </GenericPage>
    );
  }
}

export default Home;
