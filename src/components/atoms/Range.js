import React, { Component } from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const RangeComponent = Slider.Range;

class Range extends Component {

  constructor() {
    super();
    this.state = {
      value: [0, 9999]
    }
  }

  onSliderChange = (value) => {
    this.props.values(value)
    this.setState({
      value,
    });
  }

  render() {
    return (
      <RangeComponent
        step={1}
        allowCross={false}
        onChange={this.onSliderChange}
        min={0} max={9999}
        trackStyle={[{ backgroundColor: '#666', height: 1.5 }]}
        railStyle={{ height: 1.5 }}
        {...this.props}
      />
    );

  }
}

export default Range;
