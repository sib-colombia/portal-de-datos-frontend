import React, { Component } from 'react';
import { map, findIndex, setWith, get, join, find, split } from 'lodash';

class AdvancedSearch extends Component {

  constructor(props) {
    super(props);
    this.filters = {
      'Taxonomía': [
        {
          name: 'Reino',
/*
kingdom=chromista
kingdom=incertae sedis

*/
          groups: [
            { name: 'Animales', q: 'kingdom=animalia' },
            { name: 'Plantas', q: 'kingdom=plantae' },
            { name: 'Hongos', q: 'kingdom=fungi' },
            { name: 'Bacterias', q: 'kingdom=bacteria' },
            { name: 'Chromista', q: 'kingdom=chromista' },
            { name: 'Protozoos', q: 'kingdom=protozoa' },
            { name: 'Arquea', q: 'kingdom=archaea' },
            { name: 'Virus', q: 'kingdom=viruses' },
          ],
        },
        {
          name: 'Grupos de Animales',
          groups: [
            { name: 'Mamíferos', q: 'class=Mammalia' },
            { name: 'Aves', q: 'class=Aves' },
            { name: 'Reptiles', q: 'class=Reptilia' },
            { name: 'Anfibios', q: 'class=Amphibia' },
            { name: 'Peces', q: 'class=Actinopterygii&class=Sarcopterygii&class=Elasmobranchii&class=Chondrichthyes' },
            { name: 'Insectos', q: 'class=Insecta' },
            { name: 'Escarabajos', q: 'order=Coleoptera' },
            { name: 'Mariposas', q: 'order=Lepidoptera' },
            { name: 'Hormigas', q: 'family=Formicidae' },
            { name: 'Abejas', q: 'family=Apidae&family=Colletidae&family=Halictidae' },
            { name: 'Moscas y zancudos', q: 'order=Diptera' },
            { name: 'Arácnidos', q: 'class=Arachnida' },
            { name: 'Moluscos', q: 'phylum=Mollusca' },
            { name: 'Decápodos', q: 'order=Decapoda' },
            { name: 'Equinodermos', q: 'phylum=Echinodermata' },
            { name: 'Esponjas', q: 'phylum=Porifera' },
            { name: 'Corales y afines', q: 'class=Anthozoa&class=Hydrozoa' },
          ],
        },
        {
          name: 'Grupos de Plantas',
          groups: [
            { name: 'Orquídeas', q: 'family=Orchidaceae' },
            { name: 'Magnolias y afines', q: 'family=Magnoliaceae&family=Podocarpaceae&family=Myristicaceae' },
            { name: 'Palmas', q: 'family=Arecaceae' },
            { name: 'Frailejones', q: 'genus=Carramboa&genus=Coespeletia&genus=Espeletia,Espeletiopsis&genus=Libanothamnus&genus=Paramiflos&genus=Ruilopezia,Tamania' },
            { name: 'Cactus', q: 'family=Cactaceae' },
            { name: 'Bromelias, labiadas y pasifloras', q: 'family=Bromeliaceae&family=Labiatae&family=Passifloraceae' },
            { name: 'Fanerógamas', q: 'family=Chrysobalanaceae&family=Dichapetalaceae&family=Lecythidaceae' },
            { name: 'Helechos y afines', q: 'class=Polypodiopsida&class=Lycopodiopsida&class=Equisetopsida&class=Psilotopsida&class=Marattiopsida' },
            { name: 'Zamias', q: 'family=Zamiaceae' },
            { name: 'Musgos y afines', q: 'phylum=Bryophyta&phylum=Hepaticophyta&phylum=Anthocerophyta&phylum=Marchantiophyta' },
          ],
        },
      ],

      'Departamento': [
      /*
        ToDo: Pendiente
      */      
        { name: 'Amazonas', q: 'stateProvince=amazonas' },
        { name: 'Antioquia', q: 'stateProvince=antioquia' },
        { name: 'Arauca', q: 'stateProvince=arauca' },
        { name: 'Archipiélago de San Andrés, Providencia y Santa Catalina', q: 'stateProvince=archipiélago de san andres, providencia y santa catalina' },
        { name: 'Atlántico', q: 'stateProvince=atlántico' },
        { name: 'Bogotá, D.C.', q: 'stateProvince=bogotá' },
        { name: 'Bolívar', q: 'stateProvince=bolívar' },
        { name: 'Boyacá', q: 'stateProvince=boyacá' },
        { name: 'Caldas', q: 'stateProvince=caldas' },
        { name: 'Caquetá', q: 'stateProvince=caquetá' },
        { name: 'Casanare', q: 'stateProvince=casanare' },
        { name: 'Cauca', q: 'stateProvince=cauda' },
        { name: 'Cesar', q: 'stateProvince=cesar' },
        { name: 'Chocó', q: 'stateProvince=chocó' },
        { name: 'Córdoba', q: 'stateProvince=córdoba' },
        { name: 'Cundinamarca', q: 'stateProvince=cundinamarca' },
        { name: 'Guainía', q: 'stateProvince=guainía' },
        { name: 'Guaviare', q: 'stateProvince=guaviare' },
        { name: 'Huila', q: 'stateProvince=huila' },
        { name: 'La Guajira', q: 'stateProvince=guajira' },
        { name: 'Magdalena', q: 'stateProvince=magdalena' },
        { name: 'Meta', q: 'stateProvince=meta' },
        { name: 'Nariño', q: 'stateProvince=nariño' },
        { name: 'Norte de Santander', q: 'stateProvince=norte de santander' },
        { name: 'Putumayo', q: 'stateProvince=putumayo' },
        { name: 'Quindío', q: 'stateProvince=quindío' },
        { name: 'Risaralda', q: 'stateProvince=risaralda' },
        { name: 'Santander', q: 'stateProvince=santander' },
        { name: 'Sucre', q: 'stateProvince=sucre' },
        { name: 'Tolima', q: 'stateProvince=tolima' },
        { name: 'Valle del Cauca', q: 'stateProvince=valle del cauca' },
        { name: 'Vaupés', q: 'stateProvince=vaupés' },
        { name: 'Vichada', q: 'stateProvince=vichada' },
      ],
      'Base del registro': [
        { name: 'Observaciones humanas', q: 'basisOfRecord=HumanObservation' },
        { name: 'Observaciones con máquina', q: 'basisOfRecord=MachineObservation' },
        { name: 'Especímenes en colecciones', q: 'basisOfRecord=PreservedSpecimen' },
        { name: 'Especímenes vivos', q: 'basisOfRecord=LivingSpecimen' },
        { name: 'Especímenes fósiles', q: 'basisOfRecord=FossilSpecimen' },
        { name: 'Muestra', q: 'basisOfRecord=MaterialSample' },
      ],
      'Licencia': [
        { name: 'CC 1.0', q: 'license=cc 0' },
        { name: 'CC BY 4.0', q: 'license=cc by4-0' },
        { name: 'CC BY-NC 4.0', q: 'license=cc by-nc 4.0' },
      ],
      'Proyecto': [
        { name: 'Colombia BIO', q: 'project=ColombiaBIO' },
      ],
      'Multimedia': [
        { name: 'Audio', q: 'media_type=Sound' },
        { name: 'Video', q: 'media_type=MovingImage' },
        { name: 'Imagen', q: 'media_type=StillImage' },
      ],
    };

    this.search = [];
    this.state = {
      url: '/search',
      data: null,
      search: []
    }

    this.cleanFilters = this.cleanFilters.bind(this);
  }

  componentDidMount() {
    this.props.search && this.setState({ search: split((this.props.search).slice(1), '&') })
    setTimeout(() => {
      this.props.search && this.setState({ search: [] })
    }, 1000);
  }


  handleCheck(d, steps, event) {
    // console.log(event.target.checked)
    //if (this.search[d.q]) {
    // console.log("Validando ", d, " contra ", this.search)
    if (find(this.search, function (o) { return o.q === d.q }) !== undefined) {
      let i = findIndex(this.search, function (o) {
        // console.log("comparando ", o.q, " - ", d.q)
        return o.q === d.q
      }
      )
      // console.log("Se encontró el i como ", i)
      this.search.splice(i, 1)
      // console.log('BORRAR')
      // console.log(this.state.search)
    } else {
      this.search.push(d)
    }
    setWith(this.filters, steps, event.target.checked)
    const query = this.createQuery(this.search)
    window.history.replaceState('', '', query !== '' ? `/search?${query}` : '/');
    this.setState({ url: `/search?${query}` });
  }

  createQuery(data) {
    let lista = map(data, (value, key) => {
      return value.q
    })
    return join(lista, '&');
  }

  cleanFilters() {
    this.search = [];    
    window.history.replaceState('', '', this.props.path);
  }

  handleDefault(steps) {
    const check = findIndex(this.state.search, (q) => { return q === get(this.filters, steps) })
    steps.pop()
    steps.push('checked')

    if (check >= 0) {
      setWith(this.filters, steps, true)
    } else {
      setWith(this.filters, steps, false)
    }

    if (get(this.filters, steps) === true) {
      steps.pop();
      const item = get(this.filters, steps)
      if (find(this.search, function (o) { return o.q === item.q }) === undefined) {
        // console.log("Agregando ", item)
        this.search.push(item)
      }
      //this.search[item.q] =+ item;
      return true
    }
  }

  render() {
    return (
      <div id="navbar-menu" className="uk-modal-container uk-flex-top" data-uk-modal>
        <div className="uk-modal-dialog uk-margin-auto-vertical">
          <button className="uk-modal-close-default" type="button" data-uk-close />
          <div className="uk-modal-header">
            <h2 className="uk-modal-title">Filtros de búsqueda</h2>
          </div>
          <div className="uk-modal-body uk-column-1-2@s uk-column-1-4@m uk-column-divider">
            {
              map(this.filters, (principal, key) => (
                <div key={key}>
                  <div className={`uk-flex uk-flex-column uk-margin-small ${key !== 'Taxonomía' && 'uk-margin-top'}`}>
                    <h5 className="uk-margin-small-left uk-margin-small uk-text-bold">{key}
                      {key === 'Departamento' &&
                        <span
                          uk-icon="icon: warning; ratio: 0.75"
                          className="uk-margin-small-left"
                          title="Esta no es una búsqueda por coordenadas, los resultados se corresponden con el campo “Departamento” del estándar DwC documentado por el publicador."
                          data-uk-tooltip
                        />
                      }
                    </h5>
                    <div className="uk-width-1-4 uk-background-primary" style={{ height: 2 }} />
                  </div>
                  {
                    map(principal, (head, key1) => {
                      if (typeof head.groups === 'object') {
                        return <div key={key1} className="uk-flex uk-flex-column uk-margin-small">
                          <span className="uk-text-bold uk-margin-small">{head.name}</span>
                          {
                            map(head.groups, (column, key2) => {
                              const checked = this.handleDefault([key, key1, 'groups', key2, 'q'])
                              return (
                                <div key={key2}>
                                  <label style={{ cursor: 'pointer' }}>
                                    <input
                                      className="uk-checkbox"
                                      type="checkbox"
                                      defaultChecked={checked}
                                      onChange={(e) => this.handleCheck(column, [key, key1, 'groups', key2, 'checked'], e)}
                                    /> {column.name}
                                  </label><br />
                                </div>
                              )
                            })
                          }
                        </div>
                      } else {
                        const checked = this.handleDefault([key, key1, 'q'])
                        return <div key={key1}>
                          <label style={{ cursor: 'pointer' }}>
                            <input className="uk-checkbox" type="checkbox" defaultChecked={checked} onClick={(e) => this.handleCheck(head, [key, key1, 'checked'], e)} /> {head.name}
                          </label><br />
                        </div>
                      }
                    })
                  }
                </div>
              ))
            }
          </div>
          <div className="uk-modal-footer uk-text-right">
            <button className="uk-button uk-button-default uk-modal-close uk-margin-small-right" type="button">Cancelar</button>
            <button className="uk-button uk-button-default uk-margin-small-right" type="button" onClick={this.cleanFilters}>Reiniciar</button>
            <a className="uk-button uk-button-primary uk-text-bold" href={this.state.url} >Buscar</a>
          </div>
        </div>
      </div>
    )
  }
}

AdvancedSearch.id = '#navbar-menu';

export default AdvancedSearch;
