import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import cx from 'classnames';

class GenericPage extends Component {

  constructor() {
    super();
    this.state = {
      menu: true,
      browser: false
    }
  }

  componentWillMount() {
    /*
    const browser = JSON.parse(localStorage.getItem('browser'))
    if (browser === null) {
      setTimeout(() => {
        this.setState({ browser: false })
        localStorage.setItem('browser', true)
      }, 10000)
    } else {
      this.setState({ browser: false })
    }
    */
  }

  render() {
    return (
      <div className={cx('uk-scope', this.props.className)}>
        <Helmet>
          <title>{this.props.titlep}</title>
        </Helmet>
        <this.props.header.type
          menu={() => this.setState({ menu: !this.state.menu })}
          openAdvanceSearch={() => { this.setState({ menu: false }) }}
          {...this.props.header.props}
        />
        <div className="uk-background-muted" data-uk-height-viewport="expand:true;">
          {this.props.children}
        </div>
        {this.props.footer}
      </div>
    );
  }
}

export default GenericPage;
