import React, {Component, Fragment} from 'react'

class SpeciesRow extends Component {

  render() {
    const {data} = this.props;
    let x = data.hits.hits.hits[0]._source;
    console.log ("datos de api: " + x)

    return (
      <Fragment>
        { x.genus && x.specificEpithet  &&
        <tr>
        <td>{data.doc_count}</td>
        <td className="nombreespecies">{data.key}</td>
 {/*       <td>{x.kingdom}</td>
        <td>{x.phylum}</td>
        <td>{x.class}</td>
        <td>{x.order}</td>
        <td>{x.family}</td>*/}
        <td>{x.genus}</td>
        <td>{x.specificEpithet}</td>
        </tr>
        }
      </Fragment>
    );
  }
}

export default SpeciesRow;
