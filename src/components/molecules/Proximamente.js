import React, { Component } from 'react';

class Proximamente extends Component {

  render() {
    return (
      <div className="uk-text-center uk-margin">
        Próximamente
      </div>
    )
  }
}

export default Proximamente;
