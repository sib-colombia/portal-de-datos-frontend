import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

class ProvidersRow extends Component {

  render() {
    const { data } = this.props;
    let x = data.hits.hits.hits[0]._source
    
    return (
      <tr>
        <td className="uk-text-truncate"><Link className="uk-link-reset" to={`/provider/${data.key}`}>{x.organizationName}</Link></td>
        <td><Link className="uk-link-reset" to={`/provider/${data.key}`}>{<NumberFormat value={data.doc_count} displayType="text" thousandSeparator />}</Link></td>
        <td><Link className="uk-link-reset" to={`/provider/${data.key}`}>{<NumberFormat value={data.count_species} displayType="text" thousandSeparator />}</Link></td>
      </tr>
    );
  }
}

export default ProvidersRow;
