import React, { Component } from 'react';
import * as L from 'leaflet';
import { map } from 'lodash'

class LiveMap extends Component {

  constructor(props) {
    super(props)
    this.state = {
      zoom: this.props.zoom ? this.props.zoom : 6,
      center: this.props.center ? this.props.center : [4.36, -74.04],
    }

    this.data = []
    this.geoJson = []
  }

  componentDidMount() {
    const baseMaps = this.setBaseMaps()
    const { Mapnik, Cartodb, BlackAndWhite } = baseMaps
    const overlayMaps = this.setOverlayMaps()
    const { zoom, center } = this.state
    const map = this.map = L.map(this.div, {
      zoom,
      center,
      layers: [BlackAndWhite, Mapnik, Cartodb],
      attributionControl: false,
    })

    map.scrollWheelZoom.disable();
    this.map.on('click', () => { this.map.scrollWheelZoom.enable(); });
    this.map.on('mouseout', () => { this.map.scrollWheelZoom.disable(); });

    L.control.layers(baseMaps, overlayMaps).addTo(this.map)
  }

  componentWillUnmount() {
    this.map = null
    this.data = []
    this.geoJson = []
  }

  componentWillReceiveProps(nextProps) {
    const { geoJson, data } = nextProps

    this.map.eachLayer((layer) => {
      //console.log(layer)
      if (layer._featureGroup || layer._bounds)
        this.map.removeLayer(layer)
    })

    if (data !== null && Object.values(geoJson).length > 0) {
      this.setGeoJson(Object.values(geoJson), this.geoJson)
    }

    if (data !== null) {
      this.setClusterLayer(data, this.data)
    }
  }

  setBaseMaps() {
    return {
      Mapnik: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' }),
      Cartodb: L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png'),
      BlackAndWhite: L.tileLayer('https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' }),
    }
  }

  setOverlayMaps() {
    return {
      'Límite departamental de Colombia': L.tileLayer.wms('http://mapas.parquesnacionales.gov.co/services/pnn/wms', { layers: 'pnn:departamentos', format: 'image/png', transparent: true, attribution: '&copy; Parques Nacionales' }),
      'Límite municipal de Colombia': L.tileLayer.wms('http://mapas.parquesnacionales.gov.co/services/pnn/wms', { layers: 'pnn:municipios', format: 'image/png', transparent: true, attribution: '&copy; Parques Nacionales' }),
      'Parques Nacionales Naturales de Colombia': L.tileLayer.wms('http://mapas.parquesnacionales.gov.co/services/pnn/wms', { layers: 'pnn:area_protegida', format: 'image/png', transparent: true, attribution: '&copy; Parques Nacionales' }),
      'Otras Áreas protegidas': L.tileLayer.wms('http://mapas.parquesnacionales.gov.co/services/pnn/wms', { layers: 'pnn:otras_areas_sinap', format: 'image/png', transparent: true, attribution: '&copy; Parques Nacionales' }),
      'Reservas Naturales de la Sociedad Civil': L.tileLayer.wms('http://mapas.parquesnacionales.gov.co/services/pnn/wms', { layers: 'pnn:rnsc', format: 'image/png', transparent: true, attribution: '&copy; Parques Nacionales' }),
      'Corporaciones Autónomas regionales': L.tileLayer.wms('http://mapas.parquesnacionales.gov.co/services/pnn/wms', { layers: 'pnn:car', format: 'image/png', transparent: true, attribution: '&copy; Parques Nacionales' }),
    }
  }

  setGeoJson(data) {
    map(data, (v, k) => {
      L.geoJSON(v, {
        style: { color: '#ff7847', weight: 1.3, opacity: 0.9 }
      }).addTo(this.map)
    })
  }

  setClusterLayer(data, prev) {
    let markers = L.layerGroup(/* {
      iconCreateFunction: (cluster) => {
				var markers = cluster.getAllChildMarkers();
				var n = 0;
				for (var i = 0; i < markers.length; i++) {
					n += markers[i].number;
				}
				return L.divIcon({ html: n, className: 'mycluster', iconSize: L.point(40, 40) });
			},
    } */)
    //this.data = []
    //console.log(data)
    map(data, (v, k) => {
      const marker = L.circle(v.position, {radius: v.data.count})
      //const marker = L.marker(v.position)
        .on('click', () => this.openPopup(v.data))

      markers.addLayer(marker)
    })

    this.map.addLayer(markers)
  }

  openPopup(data) {
    this.props.popup(data)
  }

  render() {
    return (
      <div ref={el => this.div = el} style={{ height: 'calc(100vh - 230px)' }}></div>
    )
  }
}

export default LiveMap;
