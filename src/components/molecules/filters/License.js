import React, { Component } from 'react'
import { map, differenceWith, isEqual, findKey, isEmpty, split, findIndex } from 'lodash'
import NumberFormat from 'react-number-format'

import Filters from '../Filters'
import * as FacetService from '../../../services/FacetService'

const xhr = new XMLHttpRequest()

class License extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: null,
      search: props.search,
      modified: false   
    }

    this.query = []
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.setState({ search: this.props.search }, () => {
      this.fillValues('', decodeURI(this.state.search))
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.search !== this.props.search) {
      this.setState({ search: nextProps.search }, () => {
        const search = split(this.state.search, '&')
        let nSearch = []
        map(search, (v1) => {
          let insert = false
          if(findIndex(this.query, (o) => o.id === v1) < 0) 
            insert = true

          if(insert)
            nSearch.push(v1)
        })
        this.fillValues('', nSearch.join('&'))
      })
    }
  }

  fillValues(autocom, search) {
    FacetService.getFacet(xhr, 'license', autocom, search)
      .then(data => {
        this.llenarCampos(data)
      })
      .catch(() => {
        this.setState({ values: [] })
      })
  }
  
  llenarCampos(data){
      
    const total = data.hits.total
    const campos = data.aggregations.license.buckets
    
    let values = []
    
    map(campos, (v, k) => {
      //console.log("Licencia ", v),
      values.push(
        {
          id: 'license=' + v.key, 
          label:v.key,
          idQ: k, 
          labelQ: 'license', 
          value: v.doc_count, 
          fraction: parseInt(v.doc_count * 100/total, 10) 
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }
  

  handleFilter(value) {
    this.query.push(value)
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1)
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  cleanFilters() {
    this.query = []
    this.child.handleDrop(false);
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    const available = ['license']
    map(data, (v) => {
      const sp = split(v, '=')
      findIndex(available, (o) => {
        if (o === sp[0]) {
          if (this.state.values) {
            const i = findKey(this.state.values, (o) => { return o.id === sp[0] + '=' + decodeURI(sp[1]) })
            if (i >= 0) {
              this.handleFilter(this.state.values[i])
            }
          }
        }
      })
    })

    this.setState({ modified: true })
  }

  render() {
    return (
      <Filters.Base onRef={ref => { this.child = ref }} title="Licencia" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)} empty={isEmpty(this.state.values)}>
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.state.values, this.query, isEqual)), (value, key) =>
              <label
                key={key}
                className="uk-grid-collapse uk-flex-between uk-flex-middle uk-text-small"
                style={{
                  background: "url(/images/fondo.png) no-repeat",
                  backgroundSize: value.fraction + "% 100%",
                  cursor: 'pointer',
                  paddingTop: 2.5,
                  paddingBottom: 2.5,
                  paddingLeft: 2.5
                }}
                onClick={(e) => { e.preventDefault(); this.handleFilter(value)}}
                title={value.label}
              >
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" />{value.label && value.label.match(/\((.*)\)/).pop()}
                <span className="uk-float-right">
                  <NumberFormat value={value.value} displayType="text" thousandSeparator />
                </span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}
export default License;
