import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex, lowerCase, split } from 'lodash';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService';

const xhr = new XMLHttpRequest()

class Location extends Component {

  constructor(props) {
    super(props)
    this.state = {
      select: 'País',
      data: null,
      values: null,
      modified: false,
      search: props.search
    }

    this.filters = {
      'País': 'country',
      'Departamento': 'stateProvince',
      'Municipio': 'county',
      'Localidad': 'locality',
    }
    this.query = []

    this.handleSelect = this.handleSelect.bind(this)
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.setState({ search: this.props.search }, () => {
      this.fillValues('', decodeURI(this.state.search))
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.search !== this.props.search) {
        this.setState({ search: nextProps.search }, () => {
        const search = split(this.state.search, '&')
        let nSearch = []
        map(search, (v1) => {
          let insert = false
          if(findIndex(this.query, (o) => o.id === v1) < 0) 
            insert = true

          if(insert)
            nSearch.push(v1)
        })
        this.fillValues('', nSearch.join('&'))
      })
    }
  }

  handleSelect(e) {
    this.setState({
      select: e.target.value,
    }, () => this.fillValues())
  }

  fillValues(autocom, search) {
    this.setState({ values: [] }, () => {
    //this.setState({ values: [] }, () => {
      FacetService.getAuto(xhr, this.filters[this.state.select], autocom, search)
        .then(data => {
          this.llenarCampos(data, this.filters[this.state.select]);
        })
        .catch(() => {
          this.setState({ values: [] })
        })
    })
  }

  llenarCampos(data, x){
      
    console.log("El resultado de la búsqueda es: ", data)
    
    //const total = 1
    const campos = data.suggest[x+"S"][0].options
    console.log(campos)
    
    let values = []
    
    map(campos, (v, k) => {
      values.push(
        {
          id: x+'=' + v.text.trim(), 
          label: v.text, 
          idQ: k, 
          labelQ: x, 
          value: v.doc_count, 
          fraction: 0
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }
  
  handleFilter(value) {
    const i = findIndex(this.state.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.state.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.push(obj)

    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    const available = Object.values(this.filters)
    map(data, (v) => {
      const sp = split(v, '=')
      findIndex(available, (o) => {
        if (o === sp[0]) {
          if (this.state.values) {
            const i = findKey(this.state.values, (o) => { return o.id === sp[0] + '=' + decodeURI(sp[1]) })
            if (i >= 0) {
              this.handleFilter(this.state.values[i].label)
            }
          }
        }
      })
    })

    this.setState({ modified: true })
  }
  
  handleChange(e) {
    //const arg = '&' + this.filters[this.state.select] + '=' + e
    this.fillValues(e)
  }

  render() {
    return (
      <Filters.Base title="Ubicación" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <select className="uk-select uk-form-small" onChange={this.handleSelect}>
          <option>País</option>                    
          <option>Departamento</option>          
          <option>Municipio</option>
          <option>Localidad</option>
        </select>
        {/*{this.state.values && this.state.values.length > 0 
          ?*/}
            <Autocomplete 
              values={this.getValues(this.state.values)} 
              placeholder={`Escriba ${lowerCase(this.state.select)}`} 
              selectValue={(value) => this.handleFilter(value)} 
              onChange={(e) => {this.handleChange(e)}}
            />
        {/*  :
            <div className="uk-text-small uk-text-muted uk-text-center">No hay filtros disponibles.</div>            
        }*/}
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small">
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />{value.label}
                <span className="uk-float-right">{value.value}</span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    )
  }
}

export default Location;
