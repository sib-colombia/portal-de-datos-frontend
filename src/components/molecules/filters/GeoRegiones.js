import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex, lowerCase } from 'lodash';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService';

const xhr = new XMLHttpRequest()

class GeoRegiones extends Component {

  constructor() {
    super();
    this.state = {
      select: 'Continentales',
      data: null,
    }


    this.filters = {
      'Continentales': 'geo_natural_regions',
      'Marítimos': 'geo_colombia'
    };    

    this.values = [];
    this.query = [];
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  handleSelect(e) {
    this.setState({
      select: e.target.value,
    }, () => this.fillValues())
  }

  componentDidMount() {
    this.fillValues()
  }


  fillValues(autocom, search) {
    this.setState({ values: [] }, () => {
    //this.setState({ values: [] }, () => {
      FacetService.getAuto(xhr, this.filters[this.state.select], autocom, search)
        .then(data => {
          this.llenarCampos(data, this.filters[this.state.select]);
        })
        .catch(() => {
          this.setState({ values: [] })
        })
    })
  }

  llenarCampos(data, x){
      
    console.log("El resultado de la búsqueda es: ", data)
    
    //const total = 1
    const campos = data.suggest[x+"S"][0].options
    console.log(campos)
    
    let values = []
    
    map(campos, (v, k) => {
      values.push(
        {
          id: x+'=' + v.text.trim(), 
          label: v.text, 
          idQ: k, 
          labelQ: x, 
          value: v.doc_count, 
          fraction: 0
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }

  handleFilter(value) {
    const i = findIndex(this.state.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.state.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.push(obj)

    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.state.values, (o) => { return o.id === decodeURIComponent(item) })
      if (i >= 0) {
        this.handleFilter(this.state.values[i].label)
      }
    })
  }
  
  handleChange(e) {
    const arg = '&'+this.filters[this.state.select]+'=' + e
    this.fillValues(arg)
  }

  render() {
    return (
      <Filters.Base title="Regiones" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <select className="uk-select uk-form-small" onChange={this.handleSelect}>
          <option>Continentales</option>
          <option>Marítimos</option>
        </select>
        {this.state.values &&
          <Autocomplete 
            values={this.getValues(this.state.values)} 
            placeholder={`Escriba ${lowerCase(this.state.select)}`} 
            selectValue={(value) => this.handleFilter(value)} 
            onChange={(e) => {this.handleChange(e)}}
          />
        }
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small">
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />{value.label}
                <span className="uk-float-right">{value.value}</span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
/*
  constructor() {
    super();
    this.state = {
      select: 'Páramos',
      data: null,
    }

    this.values = [];
    this.filters = [];
    this.query = [];
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentWillMount() {
    const sFilters = [
      { id: 'strategic=paramo 1', label: 'Paramo 1', idQ: 'paramo 1', labelQ: 'strategic', value: 325000 },
      { id: 'strategic=paramo 2', label: 'Paramo 2', idQ: 'paramo 2', labelQ: 'strategic', value: 32500 },
      { id: 'strategic=paramo 3', label: 'Paramo 3', idQ: 'paramo 3', labelQ: 'strategic', value: 3250 },
    ]

    map(sFilters, (value) => {
      this.values.unshift(value);
      this.filters.unshift(value);
    })

    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  handleSelect(e) {
    this.setState({
      select: e.target.value,
    })
  }

  handleFilter(value) {
    const i = findIndex(this.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.values, (o) => { return o.id === decodeURIComponent(item) })
      if (i >= 0) {
        this.handleFilter(this.values[i].label)
      }
    })
  }

  render() {
    return (
      <Filters.Base title="Ecosistemas estratégicos" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <select className="uk-select uk-form-small">
          <option>Páramos</option>
          <option>Áreas importantes para la conservación de las aves</option>
          <option>Busque seco tropical</option>
        </select>
        <Autocomplete values={this.getValues(this.values)} placeholder={`Escriba el ${lowerCase(this.state.select)}`} selectValue={(value) => this.handleFilter(value)} />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          {
            map((differenceWith(this.filters, this.query, isEqual)), (value, key) =>
              <label key={key} className="uk-form-small">
                <input className="uk-checkbox uk-margin-small-right" type="checkbox" onClick={(e) => { e.preventDefault(); this.handleFilter(value) }} />{value.label}
                <span className="uk-float-right">{value.value}</span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    )
  }
  */
}

export default GeoRegiones;
