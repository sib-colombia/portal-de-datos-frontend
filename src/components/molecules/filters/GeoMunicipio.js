import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex } from 'lodash';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService';

const xhr = new XMLHttpRequest()

class GeoMunicipio extends Component {

  constructor() {
    super();
    this.state = {
      select: 'País',
      data: null,
    }

    this.query = [];
  }

  componentWillMount() {
    this.props.onRef(this)
    this.activeFilters(this.props.activeFilters)
  }

  componentDidMount() {
    this.fillValues()
  }

  fillValues(autocom, search) {
    this.setState({ values: [] }, () => {
    //this.setState({ values: [] }, () => {
      FacetService.getAuto(xhr, "geo_municipalities", autocom, search)
        .then(data => {
          this.llenarCampos(data, "geo_municipalities");
        })
        .catch(() => {
          this.setState({ values: [] })
        })
    })
  }

  llenarCampos(data, x){
      
    console.log("El resultado de la búsqueda es: ", data)
    
    //const total = 1
    const campos = data.suggest[x+"S"][0].options
    
    let values = []
    
    map(campos, (v, k) => {

      values.push(
        {
          id: x+'=' + v.text.trim(), 
          label: v.text, 
          idQ: k, 
          labelQ: x, 
          value: v.doc_count, 
          fraction: 0
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }

  handleFilter(value) {
    const i = findIndex(this.state.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.state.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.unshift(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  activeFilters(data) {
    map(data, (item) => {
      const i = findKey(this.state.values, (o) => { return o.id === decodeURIComponent(item) })
      if (i >= 0) {
        this.handleFilter(this.state.values[i].label)
      }
    })
  }

  handleChange(e) {
    //const arg = '&municipio=' + e
    this.fillValues(e)
  }

  render() {
    return (
      <Filters.Base title="Municipio" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)}>
        <Autocomplete 
          values={this.getValues(this.state.values)} 
          placeholder={`Escribe el nombre del municipio`} 
          selectValue={(value) => this.handleFilter(value)} 
          onChange={(e) => {this.handleChange(e)}} 
        />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
        { 
          map((differenceWith(this.state.values, this.query, isEqual)), (value, key) =>
            <label 
              key={key} 
              onClick={(e) => { e.preventDefault(); this.handleFilter(value) }}
              style={{ cursor: 'pointer', marginBottom: 5 }}
              className="uk-grid-collapse uk-flex-between uk-flex-middle uk-text-small" 
              data-uk-grid
              title={value.label}                
            >
              <input
                className="uk-checkbox"
                type="checkbox"
              />
              <span className="uk-width-3-4 uk-text-truncate uk-margin-small-left">{value.label}</span>
              <span className="uk-width-expand uk-text-right">{value.value}</span>
            </label>
          )
        }
        </div>
      </Filters.Base>
    )
  }
}

export default GeoMunicipio;
