import React, { Component } from 'react';
import Autocomplete from '../../atoms/Autocomplete';
import { map, differenceWith, isEqual, findKey, findIndex, split, isEmpty } from 'lodash';

import Filters from '../Filters';
import * as FacetService from '../../../services/FacetService';

const xhr = new XMLHttpRequest()

class Doi extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      modified: false,
      search: props.search
    }

    this.query = []
  }

  componentWillMount() {
    this.props.onRef(this)
  }

  componentDidMount() {
    this.setState({ search: this.props.search }, () => {
      this.fillValues('', decodeURI(this.state.search))
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.search !== this.props.search) {
        this.setState({ search: nextProps.search }, () => {
        const search = split(this.state.search, '&')
        let nSearch = []
        map(search, (v1) => {
          let insert = false
          if(findIndex(this.query, (o) => o.id === v1) < 0) 
            insert = true

          if(insert)
            nSearch.push(v1)
        })
        this.fillValues('', nSearch.join('&'))
      })
    }
  }

  fillValues(autocom, search) {
    this.setState({ values: [] }, () => {
    //this.setState({ values: [] }, () => {
      FacetService.getAuto(xhr, "doi", autocom, search)
        .then(data => {
          this.llenarCampos(data, "doi");
        })
        .catch(() => {
          this.setState({ values: [] })
        })
    })
  }

  llenarCampos(data, x){
      
    console.log("El resultado de la búsqueda es: ", data)
    
    //const total = 1
    const campos = data.suggest[x+"S"][0].options
    console.log(campos)
    
    let values = []
    
    map(campos, (v, k) => {
      values.push(
        {
          id: 'doi=' + v.text.trim(), 
          label: v.text, 
          idQ: k, 
          labelQ: x, 
          value: v.doc_count, 
          fraction: 0
        })
    })

    this.setState({ values }, () => {!this.state.modified && this.activeFilters(this.props.activeFilters)})        
  
  }
  handleFilter(value) {
    const i = findIndex(this.state.values, (o) => { return o.label === value || o.label === value.label });
    const obj = this.state.values[i];
    const item = findIndex(this.query, obj)
    if (item < 0)
      this.query.push(obj)

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  deleteFilter(value) {
    const position = findKey(this.query, value);
    this.query.splice(position, 1);

    this.props.count(this.query)
    this.setState({ data: this.query })
  }

  cleanFilters() {
    this.query = [];
    this.props.count(this.query);
    this.setState({ data: this.query })
  }

  getValues(values) {
    let filter = [];
    map(this.query, (value) => {
      filter.unshift(value)
    })
    return differenceWith(values, filter, isEqual)
  }

  activeFilters(data) {
    const available = ['doi']
    map(data, (v) => {
      const sp = split(v, '=')
      findIndex(available, (o) => {
        if (o === sp[0]) {
          if (this.state.values) {
            const i = findKey(this.state.values, (o) => { return o.id === sp[0] + '=' + decodeURI(sp[1]) })
            if (i >= 0) {
              this.handleFilter(this.state.values[i].label)
            }
          }
        }
      })
    })

    this.setState({ modified: true })
  }

  handleChange(e) {
    this.fillValues(e)
  }

  render() {
    return (
      <Filters.Base title="Doi" handlerFilter={this.state.data} func={(value) => this.deleteFilter(value)} empty={isEmpty(this.state.values)}>
        <Autocomplete 
          values={this.getValues(this.state.values)} 
          placeholder={`Escribe el nombre doi`} 
          selectValue={(value) => this.handleFilter(value)} 
          onChange={(e) => {this.handleChange(e)}} 
        />
        <div className="uk-child-width-1-1 uk-grid-collapse" data-uk-grid="">
          { 
            map((differenceWith(this.state.values, this.query, isEqual)), (value, key) =>
              <label 
                key={key} 
                onClick={(e) => { e.preventDefault(); this.handleFilter(value) }}
                style={{
                  background: "url(/images/fondo.png) no-repeat",
                  backgroundSize: value.fraction + "% 100%",
                  cursor: 'pointer',
                  paddingTop: 2.5,
                  paddingBottom: 2.5,
                  paddingLeft: 2.5
                }}
                className="uk-grid-collapse uk-flex-between uk-flex-middle uk-text-small" 
                data-uk-grid
                title={value.label}                
              >
                <input
                  className="uk-checkbox"
                  type="checkbox"
                />
                <span className="uk-width-3-4 uk-text-truncate uk-margin-small-left">{value.label}:</span>
                <span className="uk-width-expand uk-text-right">{value.value}</span>
              </label>
            )
          }
        </div>
      </Filters.Base>
    );
  }
}

export default Doi;
