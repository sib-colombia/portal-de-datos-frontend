import React, { Component } from 'react';

class Taxon extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.taxonID && data.taxonID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del taxón</span>
            </div>
            <div>
              <span className="uk-text-break">{data.taxonID}</span>
            </div>
          </li>
        }
        {
          (data.scientificNameID && data.scientificNameID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del nombre científico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.scientificNameID}</span>
            </div>
          </li>
        }
        {
          (data.acceptedNameUsageID && data.acceptedNameUsageID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del nombre aceptado usado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.acceptedNameUsageID}</span>
            </div>
          </li>
        }
        {
          (data.parentNameUsageID && data.parentNameUsageID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del nombre parental usado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.parentNameUsageID}</span>
            </div>
          </li>
        }
        {
          (data.originalNameUsageID && data.originalNameUsageID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del nombre original usado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.originalNameUsageID}</span>
            </div>
          </li>
        }
        {
          (data.nameAccordingToID && data.nameAccordingToID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del nombre de acuerdo con</span>
            </div>
            <div>
              <span className="uk-text-break">{data.nameAccordingToID}</span>
            </div>
          </li>
        }
        {
          (data.namePublishedInID && data.namePublishedInID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del nombre publicado en</span>
            </div>
            <div>
              <span className="uk-text-break">{data.namePublishedInID}</span>
            </div>
          </li>
        }
        {
          (data.taxonConceptID && data.taxonConceptID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID del concepto del taxón</span>
            </div>
            <div>
              <span className="uk-text-break">{data.taxonConceptID}</span>
            </div>
          </li>
        }
        {
          (data.scientificName && data.scientificName !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre científico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.scientificName}</span>
            </div>
          </li>
        }
        {
          (data.acceptedNameUsage && data.acceptedNameUsage !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre aceptado usado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.acceptedNameUsage}</span>
            </div>
          </li>
        }
        {
          (data.parentNameUsage && data.parentNameUsage !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre parental usado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.parentNameUsage}</span>
            </div>
          </li>
        }
        {
          (data.originalNameUsage && data.originalNameUsage !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre original usado</span>
            </div>
            <div>
              <span className="uk-text-break">{data.originalNameUsage}</span>
            </div>
          </li>
        }
        {
          (data.nameAccordingTo && data.nameAccordingTo !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre de acuerdo con</span>
            </div>
            <div>
              <span className="uk-text-break">{data.nameAccordingTo}</span>
            </div>
          </li>
        }
        {
          (data.namePublishedIn && data.namePublishedIn !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre publicado en</span>
            </div>
            <div>
              <span className="uk-text-break">{data.namePublishedIn}</span>
            </div>
          </li>
        }
        {
          (data.namePublishedInYear && data.namePublishedInYear !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre publicado en el año</span>
            </div>
            <div>
              <span className="uk-text-break">{data.namePublishedInYear}</span>
            </div>
          </li>
        }
        {
          (data.higherClassification && data.higherClassification !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Clasificación superiorkingdom</span>
            </div>
            <div>
              <span className="uk-text-break">{data.higherClassification}</span>
            </div>
          </li>
        }
        {
          (data.kingdom && data.kingdom !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Reino</span>
            </div>
            <div>
              <span className="uk-text-break">{data.kingdom}</span>
            </div>
          </li>
        }
        {
          (data.phylum && data.phylum !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Filo</span>
            </div>
            <div>
              <span className="uk-text-break">{data.phylum}</span>
            </div>
          </li>
        }
        {
          (data.class && data.class !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Clase</span>
            </div>
            <div>
              <span className="uk-text-break">{data.class}</span>
            </div>
          </li>
        }
        {
          (data.order && data.order !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Orden</span>
            </div>
            <div>
              <span className="uk-text-break">{data.order}</span>
            </div>
          </li>
        }
        {
          (data.family && data.family !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Familia</span>
            </div>
            <div>
              <span className="uk-text-break">{data.family}</span>
            </div>
          </li>
        }
        {
          (data.genus && data.genus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Género</span>
            </div>
            <div>
              <span className="uk-text-break">{data.genus}</span>
            </div>
          </li>
        }
        {
          (data.subgenus && data.subgenus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Subgénero</span>
            </div>
            <div>
              <span className="uk-text-break">{data.subgenus}</span>
            </div>
          </li>
        }
        {
          (data.specificEpithet && data.specificEpithet !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Epíteto específico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.specificEpithet}</span>
            </div>
          </li>
        }
        {
          (data.infraspecificEpithet && data.infraspecificEpithet !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Epíteto infraespecífico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.infraspecificEpithet}</span>
            </div>
          </li>
        }
        {
          (data.taxonRank && data.taxonRank !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Categoría del taxón</span>
            </div>
            <div>
              <span className="uk-text-break">{data.taxonRank}</span>
            </div>
          </li>
        }
        {
          (data.verbatimTaxonRank && data.verbatimTaxonRank !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Categoría original del taxón</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimTaxonRank}</span>
            </div>
          </li>
        }
        {
          (data.scientificNameAuthorship && data.scientificNameAuthorship !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Autoría del nombre científico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.scientificNameAuthorship}</span>
            </div>
          </li>
        }
        {
          (data.vernacularName && data.vernacularName !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Nombre común</span>
            </div>
            <div>
              <span className="uk-text-break">{data.vernacularName}</span>
            </div>
          </li>
        }
        {
          (data.nomenclaturalCode && data.nomenclaturalCode !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Código nomenclatural</span>
            </div>
            <div>
              <span className="uk-text-break">{data.nomenclaturalCode}</span>
            </div>
          </li>
        }
        {
          (data.taxonomicStatus && data.taxonomicStatus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Estado taxonómico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.taxonomicStatus}</span>
            </div>
          </li>
        }
        {
          (data.nomenclaturalStatus && data.nomenclaturalStatus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Estado nomenclatural</span>
            </div>
            <div>
              <span className="uk-text-break">{data.nomenclaturalStatus}</span>
            </div>
          </li>
        }
        {
          (data.taxonRemarks && data.taxonRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios del taxón</span>
            </div>
            <div>
              <span className="uk-text-break">{data.taxonRemarks}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Taxon;
