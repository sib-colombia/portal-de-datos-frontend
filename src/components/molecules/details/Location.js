import React, { Component } from 'react';

class Location extends Component {
  render() {
    const { data } = this.props;
    return (
      <ul className="uk-list uk-list-striped striped uk-margin-top">
        {
          (data.locationID && data.locationID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de la ubicación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.locationID}</span>
            </div>
          </li>
        }
        {
          (data.higherGeographyID && data.higherGeographyID !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">ID de la geografía superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.higherGeographyID}</span>
            </div>
          </li>
        }
        {
          (data.higherGeography && data.higherGeography !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Geografía superior</span>
            </div>
            <div>
              <span className="uk-text-break">{data.higherGeography}</span>
            </div>
          </li>
        }
        {
          (data.continent && data.continent !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Continente</span>
            </div>
            <div>
              <span className="uk-text-break">{data.continent}</span>
            </div>
          </li>
        }
        {
          (data.waterBody && data.waterBody !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Cuerpo de agua</span>
            </div>
            <div>
              <span className="uk-text-break">{data.waterBody}</span>
            </div>
          </li>
        }
        {
          (data.islandGroup && data.islandGroup !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Grupo de islas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.islandGroup}</span>
            </div>
          </li>
        }
        {
          (data.island && data.island !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Isla</span>
            </div>
            <div>
              <span className="uk-text-break">{data.island}</span>
            </div>
          </li>
        }
        {
          (data.country && data.country !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">País</span>
            </div>
            <div>
              <span className="uk-text-break">{data.country}</span>
            </div>
          </li>
        }
        {
          (data.countryCode && data.countryCode !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Código del país</span>
            </div>
            <div>
              <span className="uk-text-break">{data.countryCode}</span>
            </div>
          </li>
        }
        {
          (data.stateProvince && data.stateProvince !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Departamento</span>
            </div>
            <div>
              <span className="uk-text-break">{data.stateProvince}</span>
            </div>
          </li>
        }
        {
          (data.county && data.county !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Municipio</span>
            </div>
            <div>
              <span className="uk-text-break">{data.county}</span>
            </div>
          </li>
        }
        {
          (data.municipality && data.municipality !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Centro poblado / Cabecera municipal</span>
            </div>
            <div>
              <span className="uk-text-break">{data.municipality}</span>
            </div>
          </li>
        }
        {
          (data.locality && data.locality !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Localidad</span>
            </div>
            <div>
              <span className="uk-text-break">{data.locality}</span>
            </div>
          </li>
        }
        {
          (data.verbatimLocality && data.verbatimLocality !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Localidad original</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimLocality}</span>
            </div>
          </li>
        }
        {
          (data.verbatimElevation && data.verbatimElevation !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Elevación original</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimElevation}</span>
            </div>
          </li>
        }
        {
          (data.minimumElevationInMeters && data.minimumElevationInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Elevación mínima en metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.minimumElevationInMeters}</span>
            </div>
          </li>
        }
        {
          (data.maximumElevationInMeters && data.maximumElevationInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Elevación máxima en metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.maximumElevationInMeters}</span>
            </div>
          </li>
        }
        {
          (data.verbatimDepth && data.verbatimDepth !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Profundidad original</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimDepth}</span>
            </div>
          </li>
        }
        {
          (data.minimumDepthInMeters && data.minimumDepthInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Profundidad mínima en metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.minimumDepthInMeters}</span>
            </div>
          </li>
        }
        {
          (data.maximumDepthInMeters && data.maximumDepthInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Profundidad máxima en metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.maximumDepthInMeters}</span>
            </div>
          </li>
        }
        {
          (data.minimumDistanceAboveSurfaceInMeters && data.minimumDistanceAboveSurfaceInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Distancia mínima de la superficie metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.minimumDistanceAboveSurfaceInMeters}</span>
            </div>
          </li>
        }
        {
          (data.maximumDistanceAboveSurfaceInMeters && data.maximumDistanceAboveSurfaceInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Distancia máxima de la superficie metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.maximumDistanceAboveSurfaceInMeters}</span>
            </div>
          </li>
        }
        {
          (data.locationAccordingTo && data.locationAccordingTo !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Ubicación de acuerdo con</span>
            </div>
            <div>
              <span className="uk-text-break">{data.locationAccordingTo}</span>
            </div>
          </li>
        }
        {
          (data.locationRemarks && data.locationRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios de la ubicación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.locationRemarks}</span>
            </div>
          </li>
        }
        {
          (data.verbatimCoordinates && data.verbatimCoordinates !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Coordenadas originales</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimCoordinates}</span>
            </div>
          </li>
        }
        {
          (data.verbatimLatitude && data.verbatimLatitude !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Latitud original</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimLatitude}</span>
            </div>
          </li>
        }
        {
          (data.verbatimLongitude && data.verbatimLongitude !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Longitud original</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimLongitude}</span>
            </div>
          </li>
        }
        {
          (data.verbatimCoordinateSystem && data.verbatimCoordinateSystem !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Sistema original de coordenadas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimCoordinateSystem}</span>
            </div>
          </li>
        }
        {
          (data.verbatimSRS && data.verbatimSRS !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">SRS original</span>
            </div>
            <div>
              <span className="uk-text-break">{data.verbatimSRS}</span>
            </div>
          </li>
        }
        {
          (data.decimalLatitude && data.decimalLatitude !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Latitud decimal</span>
            </div>
            <div>
              <span className="uk-text-break">{data.decimalLatitude}</span>
            </div>
          </li>
        }
        {
          (data.decimalLongitude && data.decimalLongitude !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Longitud decimal</span>
            </div>
            <div>
              <span className="uk-text-break">{data.decimalLongitude}</span>
            </div>
          </li>
        }
        {
          (data.geodeticDatum && data.geodeticDatum !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Datum geodésico</span>
            </div>
            <div>
              <span className="uk-text-break">{data.geodeticDatum}</span>
            </div>
          </li>
        }
        {
          (data.coordinateUncertaintyInMeters && data.coordinateUncertaintyInMeters !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Incertidumbre de las coordenadas en metros</span>
            </div>
            <div>
              <span className="uk-text-break">{data.coordinateUncertaintyInMeters}</span>
            </div>
          </li>
        }
        {
          (data.coordinatePrecision && data.coordinatePrecision !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Precisión de las coordenadas</span>
            </div>
            <div>
              <span className="uk-text-break">{data.coordinatePrecision}</span>
            </div>
          </li>
        }
        {
          (data.pointRadiusSpatialFit && data.pointRadiusSpatialFit !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Ajuste espacial del radio-punto</span>
            </div>
            <div>
              <span className="uk-text-break">{data.pointRadiusSpatialFit}</span>
            </div>
          </li>
        }
        {
          (data.footprintWKT && data.footprintWKT !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">WKT footprint</span>
            </div>
            <div>
              <span className="uk-text-break">{data.footprintWKT}</span>
            </div>
          </li>
        }
        {
          (data.footprintSRS && data.footprintSRS !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">SRS footprint</span>
            </div>
            <div>
              <span className="uk-text-break">{data.footprintSRS}</span>
            </div>
          </li>
        }
        {
          (data.footprintSpatialFit && data.footprintSpatialFit !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Ajuste espacial de footprint</span>
            </div>
            <div>
              <span className="uk-text-break">{data.footprintSpatialFit}</span>
            </div>
          </li>
        }
        {
          (data.georeferencedBy && data.georeferencedBy !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Georreferenciado por</span>
            </div>
            <div>
              <span className="uk-text-break">{data.georeferencedBy}</span>
            </div>
          </li>
        }
        {
          (data.georeferencedDate && data.georeferencedDate !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fecha de georreferenciación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.georeferencedDate}</span>
            </div>
          </li>
        }
        {
          (data.georeferenceProtocol && data.georeferenceProtocol !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Protocolo de georreferenciación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.georeferenceProtocol}</span>
            </div>
          </li>
        }
        {
          (data.georeferenceSources && data.georeferenceSources !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Fuentes de georreferenciación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.georeferenceSources}</span>
            </div>
          </li>
        }
        {
          (data.georeferenceVerificationStatus && data.georeferenceVerificationStatus !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Estado de la verificación de la georreferenciación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.georeferenceVerificationStatus}</span>
            </div>
          </li>
        }
        {
          (data.georeferenceRemarks && data.georeferenceRemarks !== '') &&
          <li className="uk-grid-collapse uk-child-width-1-2" data-uk-grid>
            <div>
              <span className="uk-text-bold">Comentarios de la georreferenciación</span>
            </div>
            <div>
              <span className="uk-text-break">{data.georeferenceRemarks}</span>
            </div>
          </li>
        }
      </ul>
    )
  }
}

export default Location;
