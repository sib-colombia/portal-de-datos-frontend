import React, { Component } from 'react';
import { union, indexOf, fill, map } from 'lodash';

class ExtensionTable extends Component {


  render() {
    let { titulo, data } = this.props;

    
    
    if(data===undefined || data===null) return <div/>;
    
    data = data[0]
    
    let traduccion = {
"accessRights": "Derechos de acceso", 
"aspect": "Dirección de la parcela", 
"audience": "Audiencia", 
"bed": "Capa",
"bibliographicCitation": "Citación bibliográfica", 
"continent": "Continente", 
"contributor": "Parte asociada", 
"coordinatePrecision": "Precisión de las coordenadas", 
"coordinateUncertaintyInMeters": "Incertidumbre de las coordenadas en metros", 
"countryCode": "Código del país", 
"country": "País", 
"county": "Municipio", 
"coverAlgaeInPercentage": "Porcentaje de cobertura de algas", 
"coverCryptogamsInPercentage": "Porcentaje de cobertura de criptógamas", 
"coverHerbsInPercentage": "Porcentaje de cobertura herbácea", 
"coverLichensInPercentage": "Porcentaje de cobertura de líquenes", 
"coverLitterInPercentage": "Porcentaje de cobertura de hojarasca", 
"coverMossesInPercentage": "Porcentaje de cobertura de musgos", 
"coverRockInPercentage": "Porcentaje de cobertura de rocas", 
"coverShrubsInPercentage": "Porcentaje de cobertura arbustiva", 
"coverTotalInPercentage": "Porcentaje de cobertura total", 
"coverTreesInPercentage": "Porcentaje de cobertura arbórea", 
"coverWaterInPercentage": "Porcentaje de cobertura de agua", 
"created": "Fecha de creación", 
"creator": "Autor", 
"dataGeneralizations": "Información retenida", 
"datasetID": "ID del conjunto de datos", 
"datasetName": "ID del conjunto de datos", 
"date": "Fecha", 
"day": "Día", 
"decimalLatitude": "Latitud decimal", 
"decimalLongitude": "Longitud decimal", 
"description": "Descripción", 
"dynamicProperties": "Generalización de los datos", 
"earliestAgeOrLowestStage": "Edad temprana o piso inferior", 
"earliestEonOrLowestEonothem": "Eón temprano o eonotema inferior", 
"earliestEpochOrLowestSeries": "Época temprana o serie inferior", 
"earliestEraOrLowestErathem": "Era temprana o eratema inferior", 
"earliestPeriodOrLowestSystem": "Periodo temprano o sistema inferior", 
"endDayOfYear": "Día final del año", 
"eventDate": "Fecha del evento", 
"eventRemarks": "Comentarios del evento", 
"eventTime": "Hora del evento", 
"fieldNotes": "Notas de campo", 
"fieldNumber": "Número de campo", 
"footprintSpatialFit": "Ajuste espacial de footprint", 
"footprintSRS": "SRS footprint", 
"footprintWKT": "WKT footprint", 
"format": "Formato", 
"formation": "Formación", 
"geodeticDatum": "Datum geodésico", 
"geologicalContextID": "ID del contexto geológico", 
"georeferencedBy": "Georreferenciado por", 
"georeferencedDate": "Fecha de georreferenciación", 
"georeferenceProtocol": "Protocolo de georreferenciación", 
"georeferenceRemarks": "Comentarios de la georreferenciación", 
"georeferenceSources": "Fuentes de georreferenciación", 
"georeferenceVerificationStatus": "Estado de la verificación de la georreferenciación", 
"group": "Grupo", 
"habitat": "Hábitat", 
"herbLayerHeightInCentimeters": "Altura de la capa herbácea en metros", 
"higherGeography": "Geografía superior", 
"higherGeographyID": "ID de la geografía superior", 
"highestBiostratigraphicZone": "Zona bioestratigráfica superior", 
"identifier": "Identificador", 
"inclinationInDegrees": "Inclicación en grados", 
"informationWithheld": "Código de la institución propietaria", 
"institutionCode": "Código de la institución", 
"institutionID": "ID de la institución", 
"islandGroup": "Grupo de islas", 
"island": "Isla", 
"language": "Idioma", 
"latestAgeOrHighestStage": "Edad tardía o piso superior", 
"latestEonOrHighestEonothem": "Eón tardío o eonotema superior", 
"latestEpochOrHighestSeries": "Época tardía o serie superior", 
"latestEraOrHighestErathem": "Era tardía o eratema superior", 
"latestPeriodOrHighestSystem": "Periodo tardío o sistema superior", 
"license": "Licencia", 
"lichensIdentified": "Líquenes identificados", 
"lithostratigraphicTerms": "Términos litoestratigráficos", 
"locality": "Localidad", 
"locationAccordingTo": "Ubicación de acuerdo con", 
"locationID": "Propiedades dinámicas", 
"locationRemarks": "Comentarios de la ubicación", 
"lowestBiostratigraphicZone": "Zona bioestratigráfica inferior", 
"maximumDepthInMeters": "Profundidad máxima en metros", 
"maximumDistanceAboveSurfaceInMeters": "Distancia máxima de la superficie metros", 
"maximumElevationInMeters": "Elevación máxima en metros", 
"measurementAccuracy": "Precisión", 
"measurementDeterminedBy": "Determinado por", 
"measurementDeterminedDate": "Fecha", 
"measurementID": "Identificador", 
"measurementMethod": "Método", 
"measurementRemarks": "Comentarios", 
"measurementTypeID": "ID del tipo", 
"measurementType": "Tipo", 
"measurementUnitID": "ID de la unidad de medida", 
"measurementUnit": "Unidad de medida", 
"measurementValueID": "ID del valor", 
"measurementValue": "Valor", 
"member": "Miembro", 
"minimumDepthInMeters": "Profundidad mínima en metros", 
"minimumDistanceAboveSurfaceInMeters": "Distancia mínima de la superficie metros", 
"minimumElevationInMeters": "Elevación mínima en metros", 
"modified": "Modificado", 
"month": "Mes", 
"mossesIdentified": "Musgos identificados", 
"municipality": "Centro poblado / Cabecera municipal", 
"occurrenceID": "ID del registro biológico", 
"ownerInstitutionCode": "Nombre del conjunto de datos", 
"parentEventID": "ID del evento parental", 
"pointRadiusSpatialFit": "Ajuste espacial del radio-punto", 
"project": "Proyecto", 
"publisher": "Editor", 
"references": "Referencias", 
"relatedResourceID": "ID del recurso relacionado", 
"relationshipAccordingTo": "Relación de acuerdo a", 
"relationshipEstablishedDate": "Fecha de relación", 
"relationshipOfResource": "Relación de recursos", 
"relationshipRemarks": "Comentarios", 
"resourceID ": "ID del recurso", 
"resourceRelationshipID": "ID de relación", 
"rights": "Derechos de autor", 
"rightsHolder": "Titular de los derechos", 
"sampleSizeUnit": "Unidad del tamaño", 
"sampleSizeValue": "Tamaño de la muestra", 
"samplingEffort": "Esfuerzo de muestreo", 
"samplingProtocol": "Protocolo de muestreo", 
"shrubLayerHeightInMeters": "Altura de la capa arbustiva en metros", 
"source": "Fuente", 
"startDayOfYear": "Día inicial del año", 
"stateProvince": "Departamento", 
"subject": "Palabras clave", 
"syntaxonName": "Descripción de la parcela", 
"taxonRemarks": "Comentarios del taxón", 
"title": "Título", 
"treeLayerHeightInMeters": "Altura de la capa arbórea en metros", 
"type": "Tipo", 
"verbatimCoordinates": "Coordenadas originales", 
"verbatimCoordinateSystem": "Sistema original de coordenadas", 
"verbatimDepth": "Profundidad original", 
"verbatimElevation": "Elevación original", 
"verbatimEventDate": "Fecha original del evento", 
"verbatimLatitude": "Latitud original", 
"verbatimLocality": "Localidad original", 
"verbatimLongitude": "Longitud original", 
"verbatimSRS": "SRS original", 
"waterBody": "Cuerpo de agua", 
"year": "Año" 

}
    let header;
    let body = [];
    map(data, (value, key) => {
      let ks = Object.keys(value)
      if (ks.indexOf("hasLocation")!==-1)
        ks.splice(ks.indexOf("hasLocation"), 1);
      if (ks.indexOf("id")!==-1)
        ks.splice(ks.indexOf("id"), 1);
      header = union(header, ks)
      body.push(fill(Array(header.length), ''))
      map(value, (v, k) => {
        body[key][indexOf(header, k)] = v;
      })
    })
  
    return (
      <div >
        <h3>{titulo}</h3>

        <div className="uk-overflow-auto uk-margin-top uk-height-max-large">
          <table className="uk-card uk-card-default uk-table uk-table-divider uk-table-small">
            <thead>
              <tr>
                {
                  map(header, (title, key) => (
                    <th key={key} className="uk-width-small">{traduccion[title]}</th>
                  ))
                }
              </tr>
            </thead>
            <tbody>
              {
                map(body, (value, key) => (
                  <tr key={key}>
                    {
                      map(value, (v, k) => 
                        {
                        if(typeof v === 'string')
                          return <td key={k} className="uk-text-break">
                            {
                              v && typeof v === 'string' &&
                              v.match(/^http.*$/) ? <a href={v} target="_blank" rel="noopener noreferrer">{v}</a> : v
                            }
                          </td>
                        else
                          return <td key={k} className="uk-text-break"></td>
                        }
                      )
                    }
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default ExtensionTable;
