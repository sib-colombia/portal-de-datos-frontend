import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ProvidersListItem extends Component {
  render() {
    const {data} = this.props;
    let x = data.platform.hits.hits[0]._source
    return (
      <div className="uk-heading-divider uk-padding-small">
        <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid>
          <div className="uk-width-1-4">
            <Link to={`/provider/${data.key}`}><img className="uk-preserved-width" src={x.resourceLogoUrl} alt="" /></Link>
          </div>
          <div>
            <h3 className="uk-text-tertiary uk-text-truncate"><Link className="uk-link-reset" to={`/provider/${data.key}`}>{x.organizationName}</Link></h3>
            <p className="uk-heading-divider">{x.organizationDescription.substr(0, 300)} ...</p>
            <div className=" uk-child-width-1-2 uk-grid-small uk-child-width-auto@s" data-uk-grid>
              <Link to={`/provider/${data.key}`} className="uk-text-tertiary" style={{ textDecoration: 'none' }}>{data.deparment}</Link>
              <Link to={`/provider/${data.key}`} className="uk-text-tertiary" style={{ textDecoration: 'none' }}><span className="uk-text-bold">{data.count_resource}</span> RECURSOS</Link>
              <Link to={`/provider/${data.key}`} className="uk-text-tertiary" style={{ textDecoration: 'none' }}><span className="uk-text-bold">{data.doc_count}</span> REGISTROS</Link>
              <Link to={`/provider/${data.key}`} className="uk-text-tertiary" style={{ textDecoration: 'none' }}><span className="uk-text-bold">{data.count_species}</span> ESPECIES</Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ProvidersListItem;
