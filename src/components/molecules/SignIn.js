import React, { Component } from 'react'
import cx from 'classnames'

import { signin, me } from '../../auth'

class SignIn extends Component {

  constructor() {
    super()
    this.state = {
      username: '',
      password: '',
      email: '',
      error: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    const name = e.target.name
    const value = e.target.value

    this.setState({ [name]: value })
  }

  handleSubmit(e) {
    e.preventDefault()
    signin(this.state)
      .then(data => {
        console.log("Intento de ingreso", data)
        if (data.error)
          this.setState({ error: data.error })
        else
          me()
            .then(user => {
              window.location.reload()
            })
      })
  }

  render() {
    return (
      <li>
        <form className="uk-form-stacked">
          {this.state.error !== '' &&
            <div
              className="uk-text-danger uk-text-small uk-text-bold uk-background-muted uk-border-rounded uk-flex uk-flex-middle uk-flex-center"
              style={{ padding: 10 }}
            >
              <span uk-icon="icon: warning" className="uk-margin-small-right" />
              {this.state.error}
            </div>
          }
          <div className="uk-margin-small">
            <label className="uk-form-label" htmlFor="username">Usuario o correo electrónico</label>
            <div className="uk-form-controls">
              <input className={cx('uk-input' /*, { 'uk-form-danger': this.state.error }*/)} id="username" name="username" type="text" onChange={this.handleChange} autoFocus={this.state.error} />
            </div>
          </div>
          <div>
            <label className="uk-form-label" htmlFor="password">Contraseña</label>
            <div className="uk-form-controls">
              <input className={cx('uk-input'/*, { 'uk-form-danger': this.state.error }*/)} id="password" name="password" type="password" onChange={this.handleChange} />
            </div>
            <a className="uk-text-tertiary uk-text-small uk-float-right" href="mailto:sib@humboldt.org.co">¿Olvidaste tu contraseña?</a>
          </div>
          <button className="uk-margin-small-top uk-button uk-button-small uk-text-bold uk-button-primary uk-width-1" onClick={this.handleSubmit}>Ingresar</button>
        </form>
      </li>
    )
  }
}

export default SignIn;
