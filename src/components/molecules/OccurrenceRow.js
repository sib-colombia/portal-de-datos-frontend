import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { get } from 'lodash';

class OccurrenceRow extends Component {

  ubicacion(r) {
    r = get(r, 'location')
    if(r === undefined)
      return ""
       
    var t = ""
    if (r[1]) {
      const n = parseFloat(r[1], 10)
      if (n > 0)
        t += n.toFixed(2) + " N"
      else
        t += (-n).toFixed(2) + " S"
    }
    if (r[0]) {
      const n = parseFloat(r[0], 10)
      t += ", "
      if (n > 0)
        t += n.toFixed(2) + " E"
      else
        t += (-n).toFixed(2) + " W"
    }
    return t
  }

  fecha(f) {
    if (f.eventDate) {
      // let fecha = new Date(f.eventDateOrg);
      // return fecha.toISOString().substr(0, 10)
      return f.eventDate
    }
    return ""
  }

  capitalize(d) {
    let string = "";
    if (d)
      string = d.charAt(0).toUpperCase() + (d.slice(1)).toLowerCase();
    return string;
  }

  render() {
    let { data } = this.props;
    data = data._source;
    if(data.occurrenceID!==""){
      data.id = data.occurrenceID;
    }
    
    return (
      <tr>
        <td className="uk-text-nowrap">
          <Link className="uk-link-reset scientificName" to={`/occurrence/${data.id}`}>{data.scientificName}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.country}</Link>
        </td>
        <td>
          <Link className="uk-link-reset uk-text-truncate" to={`/occurrence/${data.id}`}>{data.stateProvince}</Link>
        </td>
        <td className="uk-text-nowrap">
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{this.ubicacion(data)}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.basisOfRecord}</Link>
        </td>
        <td className="uk-text-truncate">
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{this.fecha(data)}</Link>
        </td>
        <td>
          <Link className="uk-link-reset uk-text-truncate" to={`/occurrence/${data.id}`}>{data.institutionCode}</Link>
        </td>
        <td className="uk-text-truncate">
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.occurrenceID}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{this.capitalize(data.taxonRank)}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.kingdom}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.phylum}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.class}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.order}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.family}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.genus}</Link>
        </td>
        <td>
          <Link className="uk-link-reset" to={`/occurrence/${data.id}`}>{data.specificEpithet}</Link>
        </td>
      </tr>
    );
  }
}

export default OccurrenceRow;
