import React, { Component } from 'react';

import AdvancedSearch from '../atoms/AdvancedSearch';

/*const styles = {
  fontSize: '1.09rem', height: 60,
}*/

class Search extends Component {

  constructor() {
    super();
    this.state = {}
  }

  handleSearch = () => {
    window.location.href = `/search?${this.url !== null ? this.url : ''}`
  };

  handleTextFieldKeyDown = event => {
    switch (event.key) {
      case 'Enter':
        console.log('Enter')
        window.location.href = `/search?q=${event.target.value}`
        break
      case 'Escape':
        // etc...
        break
      default: break
    }
  };

  changeUrl(url) {
    this.url = url
  }

/*
            <span uk-search-icon="ratio: 0.8"></span>
            <span uk-search-icon="ratio: 0.8"></span>
            <input id="search-l" className="uk-search-input" type="search" placeholder="Buscar" style={styles} onKeyDown={this.handleTextFieldKeyDown} />
            <input id="search" className="uk-search-input" type="search" placeholder="Buscar" style={styles} onKeyDown={this.handleTextFieldKeyDown} />
            backgroundColor: '#f1f8f7', 
*/
  render() {
    return (
      <div className="uk-navbar-right uk-visible@m">
        <div className="uk-navbar-item">
          <form className="uk-search uk-search-navbar uk-hidden@l" style={{ height: "20px" }}>
            <button href={AdvancedSearch.id} className="uk-form-icon uk-form-icon-flip" uk-icon="icon: settings; ratio: 1" uk-toggle="" onClick={() => { this.props.openAdvanceSearch() }} />
          </form>
          <form className="uk-search uk-search-navbar uk-visible@l uk-width-xlarge@l" style={{ height: "20px" }}>
            <button href={AdvancedSearch.id} className="uk-form-icon uk-form-icon-flip" uk-icon="icon: settings; ratio: 1" uk-toggle="" onClick={() => { this.props.openAdvanceSearch() }} />
          </form>
        </div>
        <AdvancedSearch search={this.props.search} path={this.props.path} />
      </div>
    );
  }
}

class Responsive extends Component {
  render() {
/*
              <input className="uk-search-input" type="search" placeholder="Buscar" style={styles} autoFocus />
*/
    return (
      <div className="uk-navbar-dropdown uk-navbar-dropdown-width-2" data-uk-drop="mode: click; cls-drop: uk-navbar-dropdown; boundary: !nav">
        <div className="uk-grid-small uk-flex-middle" data-uk-grid>
          <div className="uk-width-expand">
            <form className="uk-search uk-search-navbar uk-width-1-1">
              <button href="#navbar-menu" className="uk-form-icon uk-form-icon-flip uk-navbar-dropdown-close" uk-icon="icon: settings; ratio: 1" uk-toggle="" />
            </form>
          </div>
          <div className="uk-width-auto">
            <button className="uk-navbar-dropdown-close" data-uk-close />
          </div>
        </div>
      </div>
    );
  }
}

Search.Responsive = Responsive;

export default Search;
