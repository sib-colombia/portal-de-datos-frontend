import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

import Doi from '../atoms/Doi';
import License from '../atoms/License';
/*
*/

class DatasetsListItem extends Component {
  render() {
    const { data } = this.props;
    let x = data.platform.hits.hits[0]._source
    

    return (<div className="uk-heading-divider uk-padding-small">
      <div className="uk-child-width-1-1 uk-child-width-expand@s" data-uk-grid="data-uk-grid">
        <div className="uk-width-1-4">
          <Link className="uk-link-reset uk-inline" to={`/dataset/${data.key}`}>
            <img alt="" className="uk-width-1-1" src={x.resourceLogoUrl} />
          </Link>
        </div>
        <div>
          <h3 className="uk-text-tertiary uk-text-truncate">
            <Link className="uk-link-reset" to={`/dataset/${data.key}`}>{x.titleResource}</Link>
          </h3>
          <p className="uk-heading-divider box-truncate" >{x.abstract && (x.abstract).substr(0, 300)}...</p>
          <Link to={`/dataset/${data.key}`} className="uk-text-tertiary" style={{
            textDecoration: 'none'
          }}>{data.organizationName}</Link>
          <div className="uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-2@m uk-grid-small  uk-margin-small-top uk-flex uk-flex-bottom" data-uk-grid="data-uk-grid">
            <div><Doi label={x.doi} /></div>
            <div className="uk-width-auto"><License id={x.license_url} /></div>
            <div className="uk-width-auto ">
              <div className="uk-background-tertiary">
                <Link className="uk-link-reset" to={`/dataset/${data.key}`}>
                  <div className="uk-text-default uk-text-center uk-text-bold uk-margin-small-left uk-margin-small-right" style={{
                    padding: 3.5
                  }}>
                    <span><NumberFormat value={data.doc_count} displayType="text" thousandSeparator /></span>
                    <span className="uk-text-small "> REGISTROS</span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default DatasetsListItem;
