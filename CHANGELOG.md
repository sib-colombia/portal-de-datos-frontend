### Changelog 
# CHANGELOG Portal de datos
## [**2.0.1**] – (Proximamente)
### Agregado
- Módulo de descargas:
    - Descargas de registros biologicos. con base a los filtros aplicados a las búsquedas. (Filtros que se encuentren en funcionamiento)
    - Partición de consultas para descargas grandes, es decir se crean archivos parciales para descargar la memoria del servidor.
    - Envio de link de descarga al correo del usuario autenticado. El formato de descarga sera .zip.
    - Formato del correo corresponde al aprobado por la coordinación del SiB Colombia.
    - Procesamiento de una descarga a la vez por el worker que ejecuta dicho trabajo.
    - Almacenamiento y reutilización de descargas.
    - Manejo de excpeciones en todos los métodos.
#### Actualizado
- Oculta barra de búsqueda y filtros en el *Header*.

- API búsquedas:
    - Llamado a worker de descargas. 
    - Búsquedas y conteo de registros biológicos.
    - Creación de agregaciones (agrupaciones) de registros biológicos.
    - Método para el autocompletado de filtros en su versión inicial.
    - Método de busqueda geográfica.
- API consultas:
    - Consulta de register, dataset y provider específicos.
#### Solucionado
- Conteo del *Home* incluyendo todos los registros indexados.

#### Eliminado
- Sección de registros biológicos:
    - Oculta pestañas de mapa y especies. 
- Sección de publicadores:
    - Oculta columna de recurso en laa tabla.
- Sección de recursos:
    - Oculta filtros de licencia y DOI.


## [**2.0.0**] – (2018-06)
### Agregado
- sección de explorar registros biológicos:
Lista todos los registros biológicos publicados a través del SiB Colombia.
    - Filtros:
        - Taxonomía
        - Ubicación
        - Hábitat
        - Base del registro
        - Registrado por
        - ID del registro biológico
        - Publicador
        - Redes y proyectos
        - Nombre del recurso
        - Multimedia 
        - Licencia
    - Pestañas:
        - Tabla
        - Mapa
        - Expecies
        - Recursos
        - Publicadores
- Sección de explorar recursos
Lista todos los recursos publicados a través del SiB Colombia.
    - Filtros:
        - Nombre
        - Publicador
        - Redes y proyectos
        - Licencia
        - DOI
    - Pestañas:
        - Recursos
        - Registros
        - Eventos de muestreo
        - Metadados
- Sección de explorar publicadores:
Lista todos los publicadores que aportan datos al SiB Colombia.
    - Filtros: 
        - Publicador
        - Redes y proyectos
- Explorador geográfico que permite el cruce de datos con distintas capas geográficas.
    - Filtros:
        - Taxonomía
        - País
        - Departamento
        - Municipio
        - Ecosistemas
        - Regiones
        - Áreas protegidas
        - Corporaciones autónomas
 

----
## [**1.4.0**] – (2016)
### Agregado
- Nuevo API en el Portal de Datos.
- Nuevo servicio de descargas desarrollado con node.js, elastic, Kafka y docker.

#### Actualizado
- Nueva versión del API (Bambu) que soporta el Portal de Datos con la aplicación del estándar Swagger y GraphQL.
- Proporciona servicio de consultas con texto libre, agregaciones para servicios estadísticos y análisis de datos.
- Mejora del servicio de mapas basado en *tiles* vectoriales.
- Cambio del dominio de acceso al portal de datos, de data.sibcolombia.net a datos.biodiversidad.co


## [**1.3.0**] – (2015)
### Agregado
- Sistema de descargas de registros biológicos:
    - Generación de paquete de descargas.
    - Resultados de descargas se envían al correo electrónico.
    - Categorización del propósito de descarga para fines analíticos.

#### Actualizado
- Nuevas capas geográficas en el Explorador para contrastar la ubicación de los registros biológicos con respecto a ellas.


## [**1.2.0**] – (2014)
### Agregado
- Módulo con estadísticas de publicación.
- Integración con las redes sociales del SiB Colombia.
-  
#### Actualizado
- Actualización de la interfaz visual.
- Visibilización de los conjuntos de datos publicados en el marco de proyectos especiales. 
- Diseño de nuevos filtros en el portal de datos.
- Aumento de los filtros de búsqueda del Explorador Geográfico. 
- Indexador registros biológicos.


## [**1.1.0**] – (2013)
### Agregado
- Sección de ayuda -tutoriales- para consulta y descarga de información desde el portal.
- Explorador Geográfico (BETA):Ppermite la visualización geográfica de los registros publicados a través del SiB Colombia y su cruce con diferentes capas de información.
- API de consulta de registros biológicos en GeoJSON.

#### Actualizado
- Actualizaciones de la interfaz visual.
- Mejoras de usabilidad y experiencia de usuario.
- Herramientas de búsqueda y consulta: 
    - Búsqueda avanzada por municipio
    - Búsqueda por nombres comunes (e.g. halcón)


## [**1.0.0**] – (2013)
### Agregado
- Realizar búsquedas a través de los nombres de las especies, explorar por conjuntos de datos específicos o por departamentos de Colombia.


